import { helper } from '@ember/component/helper';

export function determineState(params/*, hash*/) {
  var flag = false
  if (params[1] === undefined) {
    flag = true
    return flag
  }
  else {
    var id = params[0]
    var dict = params[1]
    for (var d in dict) {
      if (d === id) {
        if (dict[d] === "1") {
          flag = false
          return flag
        }
        else {
          flag = true
          return flag
        }
      }
    }

  }

}

export default helper(determineState);
