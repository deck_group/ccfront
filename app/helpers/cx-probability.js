import { helper } from '@ember/component/helper';

export function cxProbability(params/*, hash*/) {
  var percentage=(params[0]*100).toPrecision(5)
  var percentage_str=percentage + " %"
  return percentage_str;
}

export default helper(cxProbability);
