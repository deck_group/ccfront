import { helper } from '@ember/component/helper';

export function arrayHelper(params/*, hash*/) {
  var array=[]
  if(params[0] !== undefined){
    for(var i=0 ; i<params[0].length ;i++){
      array.push(params[0][i][params[1]])
    }
    return array;
  }
}

export default helper(arrayHelper);
