import { helper } from '@ember/component/helper';

export function determineAction(params/*, hash*/) {
  var tagname = "none"
  params[0].forEach(function (data) {
    if (data.objectname.toLowerCase() === params[2].toLowerCase()) {
      if(data.nextaction.toLowerCase() === params[1].toLowerCase()){
        tagname = params[1]
      }
      
    }
  })
  return tagname;

}

export default helper(determineAction);
