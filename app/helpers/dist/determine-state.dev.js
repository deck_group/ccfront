"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.determineState = determineState;
exports["default"] = void 0;

var _helper = require("@ember/component/helper");

function determineState(params
/*, hash*/
) {
  var flag = false;

  if (params[1] === undefined) {
    flag = true;
    return flag;
  } else {
    var id = params[0];
    var dict = params[1];

    for (var d in dict) {
      if (d === id) {
        if (dict[d] === "1") {
          flag = false;
          return flag;
        } else {
          flag = true;
          return flag;
        }
      }
    }
  }
}

var _default = (0, _helper.helper)(determineState);

exports["default"] = _default;