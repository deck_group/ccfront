"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.determineAction = determineAction;
exports["default"] = void 0;

var _helper = require("@ember/component/helper");

function determineAction(params
/*, hash*/
) {
  var tagname = "none";
  params[0].forEach(function (data) {
    if (data.objectname.toLowerCase() === params[2].toLowerCase()) {
      if (data.nextaction.toLowerCase() === params[1].toLowerCase()) {
        tagname = params[1];
      }
    }
  });
  return tagname;
}

var _default = (0, _helper.helper)(determineAction);

exports["default"] = _default;