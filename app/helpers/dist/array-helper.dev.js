"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.arrayHelper = arrayHelper;
exports["default"] = void 0;

var _helper = require("@ember/component/helper");

function arrayHelper(params
/*, hash*/
) {
  var array = [];

  if (params[0] !== undefined) {
    for (var i = 0; i < params[0].length; i++) {
      array.push(params[0][i][params[1]]);
    }

    return array;
  }
}

var _default = (0, _helper.helper)(arrayHelper);

exports["default"] = _default;