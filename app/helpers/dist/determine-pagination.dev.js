"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.determinePagination = determinePagination;
exports["default"] = void 0;

var _helper = require("@ember/component/helper");

function determinePagination(params
/*, hash*/
) {
  if (params[0] === undefined) {
    return false;
  } else {
    return true;
  }
}

var _default = (0, _helper.helper)(determinePagination);

exports["default"] = _default;