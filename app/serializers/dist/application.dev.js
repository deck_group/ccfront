"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance"); }

function _iterableToArrayLimit(arr, i) { if (!(Symbol.iterator in Object(arr) || Object.prototype.toString.call(arr) === "[object Arguments]")) { return; } var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _get(target, property, receiver) { if (typeof Reflect !== "undefined" && Reflect.get) { _get = Reflect.get; } else { _get = function _get(target, property, receiver) { var base = _superPropBase(target, property); if (!base) return; var desc = Object.getOwnPropertyDescriptor(base, property); if (desc.get) { return desc.get.call(receiver); } return desc.value; }; } return _get(target, property, receiver || target); }

function _superPropBase(object, property) { while (!Object.prototype.hasOwnProperty.call(object, property)) { object = _getPrototypeOf(object); if (object === null) break; } return object; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

var ApplicationSerializer =
/*#__PURE__*/
function (_DS$JSONAPISerializer) {
  _inherits(ApplicationSerializer, _DS$JSONAPISerializer);

  function ApplicationSerializer() {
    _classCallCheck(this, ApplicationSerializer);

    return _possibleConstructorReturn(this, _getPrototypeOf(ApplicationSerializer).apply(this, arguments));
  }

  _createClass(ApplicationSerializer, [{
    key: "normalizeQueryResponse",
    value: function normalizeQueryResponse(store, clazz, payload) {
      var result = _get(_getPrototypeOf(ApplicationSerializer.prototype), "normalizeQueryResponse", this).apply(this, arguments);

      result.meta = result.meta || {};

      if (payload.links) {
        result.meta.pagination = this.createPageMeta(payload.links);
      }

      return result;
    }
  }, {
    key: "createPageMeta",
    value: function createPageMeta(data) {
      var meta = {};
      Object.keys(data).forEach(function (type) {
        var link = data[type];
        meta[type] = {};
        var a = document.createElement('a');
        a.href = link;
        a.search.slice(1).split('&').forEach(function (pairs) {
          var _pairs$split = pairs.split('='),
              _pairs$split2 = _slicedToArray(_pairs$split, 2),
              param = _pairs$split2[0],
              value = _pairs$split2[1];

          if (param == 'page%5Bnumber%5D') {
            meta[type].number = parseInt(value);
          }

          if (param == 'page%5Bsize%5D') {
            meta[type].size = parseInt(value);
          }
        });
        a = null;
      });
      var newmeta = {};

      if (Object.keys(meta['prev']).length === 0) {
        newmeta['First'] = {};
      } else {
        newmeta['First'] = meta['first'];
      }

      newmeta['Prev'] = meta['prev'];
      newmeta['Next'] = meta['next'];

      if (Object.keys(meta['next']).length === 0) {
        newmeta['Last'] = {};
      } else {
        newmeta['Last'] = meta['last'];
      }

      return newmeta;
    }
  }]);

  return ApplicationSerializer;
}(_emberData["default"].JSONAPISerializer);

exports["default"] = ApplicationSerializer;