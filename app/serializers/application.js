import DS from 'ember-data';

export default class ApplicationSerializer extends DS.JSONAPISerializer {

  normalizeQueryResponse(store, clazz, payload) {
    const result = super.normalizeQueryResponse(...arguments);
    result.meta = result.meta || {};

    if (payload.links) {
      result.meta.pagination = this.createPageMeta(payload.links);
    }
    return result;
  }
  createPageMeta(data) {
    let meta = {};
    Object.keys(data).forEach(type => {
      const link = data[type];
      meta[type] = {};
      let a = document.createElement('a');
      a.href = link;
      a.search.slice(1).split('&').forEach(pairs => {
        const [param, value] = pairs.split('=');

        if (param == 'page%5Bnumber%5D') {
          meta[type].number = parseInt(value);
        }
        if (param == 'page%5Bsize%5D') {
          meta[type].size = parseInt(value);
        }

      });
      a = null;
    });
    var newmeta = {}
    if ((Object.keys(meta['prev']).length) === 0) {
      newmeta['First'] = {}
    } else {
      newmeta['First'] = meta['first']
    }
    newmeta['Prev'] = meta['prev']
    newmeta['Next'] = meta['next']

    if ((Object.keys(meta['next']).length) === 0) {
      newmeta['Last'] = {}
    } else {
      newmeta['Last'] = meta['last']
    }
    return newmeta;

  }




}