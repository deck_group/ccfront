"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

var _environment = _interopRequireDefault(require("../config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _emberData["default"].JSONAPIAdapter.extend({
  host: _environment["default"].API_HOST,
  namespace: 'api/v1'
});

exports["default"] = _default;