import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    model(params) {
        const store = this.get('store');
        return hash({
            process: store.find('processsheduler', params.sche_id),
            jobstatus:store.find('jobstatus', params.sche_id),
            nextactions: store.query('nextallowableaction', {
                objectname: 'processsheduler',
                startstatus: 'active,deactive',
                endstatus: 'active,deactive',
                role: localStorage.getItem('role')
            }),
            notviewable: store.query('bofieldsviewnoaccess', {
                user: localStorage.getItem('user'),
                role: localStorage.getItem('role'),
                businessobject: 'processsheduler'
            }),
            id:params.sche_id,
        })

    },

    setupController(controller, model) {
        controller.set('process', model.process);
        controller.set('nextactions', model.nextactions);
        controller.set('notviewables', model.notviewable);
        controller.set('jobstatus', model.jobstatus);
        controller.set('schid',model.id);

        this._super(controller, model);
    }
});
