import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    store: service(),
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },


    model(params) {
        const store = this.get('store');
        return hash({
            data: store.query('borrower', {
                page: {
                    number: params.page,
                    size: params.size

                }
            })
        })
    },

    setupController(controller, model) {
        controller.set('borrowers', model.data);
        this._super(controller, model);
    }

});