import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },
    model(params) { 
        const store = this.get('store');
        return hash({
            data: store.query('userwiseprediction', {
                page: {
                    number: params.page,
                    size: params.size

                },
                filter:{
                    loantype: params.loantype,
                    provincename: params.province,
                    districtname: params.district
                }
                
            }),
        })
       

    },
    setupController(controller, model) { 
        controller.set('userwisepredictions', model.data);
        this._super(controller, model);
    }
});
