import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { hash } from 'rsvp';

export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    model(params) {
        const store = this.get('store');
        return hash({
            joblog: store.find('joblog', params.log_id)
        })
    },
    setupController(controller, model) {
        controller.set('joblog', model.joblog);
        this._super(controller, model);
    }

});
