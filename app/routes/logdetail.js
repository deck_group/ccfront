import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    store: service(),
    urls: service("service"),
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    model(params) {
        const store = this.get('store');
        return hash({
            joblog: store.query('joblog', {
                page: {
                    number: params.page,
                    size: params.size

                }
            })
        })
    },

    setupController(controller, model) {
        controller.set('joblogs', model.joblog);
        this._super(controller, model);
    }

});
