import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';
import $ from 'jquery';
import {
    bind
} from '@ember/runloop';

export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },

    async model(params) {
        const store = this.get('store');
        var hurl = this.get("urls.url") + 'paralog'
        var data = await $.ajax({
            method: "POST",
            url: hurl,
            data:{'time':params.timeline}
        }).then(bind(this, (data) => {
            return data["result"]
        }))
        return hash({
            predictionlogs: data
        })

    },
    setupController(controller, model) {
        controller.set('predictionlogs', model.predictionlogs);
        this._super(controller, model);
    }

});
