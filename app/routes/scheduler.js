import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    model() {
        const store = this.get('store');
        return hash({
            data: store.findAll('processsheduler'),
            jobstatus: store.findAll('jobstatus'),
            nextactions: store.query('nextallowableaction', {
                objectname: 'processsheduler',
                startstatus: 'start',
                endstatus: 'approved',
                role: localStorage.getItem('role')
            }),
            nextcrudactions: store.query('nextcrudaction', {
                objname: 'processsheduler',
                status: 'ex_approved,draft,approved,declined',
                role: localStorage.getItem('role')
            }),
            notviewable: store.query('bofieldsviewnoaccess', {
                user: localStorage.getItem('user'),
                role: localStorage.getItem('role'),
                businessobject: 'processsheduler'
            }),
        })
    },
    setupController(controller, model) {
        controller.set('processshedulers', model.data);
        controller.set('nextactions', model.nextactions);
        controller.set('nextcrudactions', model.nextcrudactions);
        controller.set('notviewables', model.notviewable);
        controller.set('jobstatuses', model.jobstatus);
        this._super(controller, model);
    }

});
