import Route from '@ember/routing/route';
import { hash } from 'rsvp';


export default Route.extend({
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },

    model(params) {
        const store = this.get('store');
        return hash({
            borrowers: store.query('borrower', {
                search: params.borrowersearch,
                page: {
                    number: params.page,
                    size: params.size

                }
            },
            

            ),
            borrowersearch:params.borrowersearch

        })
    },

    setupController(controller, model) {
        controller.set('borrowers', model.borrowers);
        controller.set('search', model.borrowersearch);
        this._super(controller, model);
    }
});
