import Route from '@ember/routing/route';
import { hash } from 'rsvp';

export default Route.extend({
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },
    model(params) {
        const store = this.get('store');
        return hash({
            borrower: store.find('borrower', params.borrowerid),
            prediction: store.query('userwiseprediction', {
                borrowerid:params.borrowerid
                
            }),
        })

    },
    setupController(controller, model) {
        controller.set('borrower', model.borrower);
        controller.set('predictions', model.prediction);
        this._super(controller, model);
    }
});
