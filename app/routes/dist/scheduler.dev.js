"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _rsvp = require("rsvp");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  store: (0, _service.inject)(),
  urls: (0, _service.inject)("service"),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem('token') === "undefined") {
      window.location = this.get("urls.signout");
    }
  },
  model: function model() {
    var store = this.get('store');
    return (0, _rsvp.hash)({
      data: store.findAll('processsheduler'),
      jobstatus: store.findAll('jobstatus'),
      nextactions: store.query('nextallowableaction', {
        objectname: 'processsheduler',
        startstatus: 'start',
        endstatus: 'approved',
        role: localStorage.getItem('role')
      }),
      nextcrudactions: store.query('nextcrudaction', {
        objname: 'processsheduler',
        status: 'ex_approved,draft,approved,declined',
        role: localStorage.getItem('role')
      }),
      notviewable: store.query('bofieldsviewnoaccess', {
        user: localStorage.getItem('user'),
        role: localStorage.getItem('role'),
        businessobject: 'processsheduler'
      })
    });
  },
  setupController: function setupController(controller, model) {
    controller.set('processshedulers', model.data);
    controller.set('nextactions', model.nextactions);
    controller.set('nextcrudactions', model.nextcrudactions);
    controller.set('notviewables', model.notviewable);
    controller.set('jobstatuses', model.jobstatus);

    this._super(controller, model);
  }
});

exports["default"] = _default;