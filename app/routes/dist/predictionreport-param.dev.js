"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _rsvp = require("rsvp");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  urls: (0, _service.inject)("service"),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem('token') === "undefined") {
      window.location = this.get("urls.signout");
    }
  },
  model: function model(params) {},
  setupController: function setupController(controller, model) {}
});

exports["default"] = _default;