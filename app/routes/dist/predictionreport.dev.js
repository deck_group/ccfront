"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _rsvp = require("rsvp");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  store: (0, _service.inject)(),
  urls: (0, _service.inject)("service"),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem('token') === "undefined") {
      window.location = this.get("urls.signout");
    }
  },
  model: function model(params) {
    var store = this.get('store');
    return (0, _rsvp.hash)({
      data: store.query('userwiseprediction', {
        page: {
          number: params.page,
          size: params.size
        }
      })
    });
  },
  setupController: function setupController(controller, model) {
    controller.set('userwisepredictions', model.data);

    this._super(controller, model);
  }
});

exports["default"] = _default;