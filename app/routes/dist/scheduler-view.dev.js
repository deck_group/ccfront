"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _rsvp = require("rsvp");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  urls: (0, _service.inject)("service"),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem('token') === "undefined") {
      window.location = this.get("urls.signout");
    }
  },
  model: function model(params) {
    var store = this.get('store');
    return (0, _rsvp.hash)({
      process: store.find('processsheduler', params.sche_id),
      jobstatus: store.find('jobstatus', params.sche_id),
      nextactions: store.query('nextallowableaction', {
        objectname: 'processsheduler',
        startstatus: 'active,deactive',
        endstatus: 'active,deactive',
        role: localStorage.getItem('role')
      }),
      notviewable: store.query('bofieldsviewnoaccess', {
        user: localStorage.getItem('user'),
        role: localStorage.getItem('role'),
        businessobject: 'processsheduler'
      }),
      id: params.sche_id
    });
  },
  setupController: function setupController(controller, model) {
    controller.set('process', model.process);
    controller.set('nextactions', model.nextactions);
    controller.set('notviewables', model.notviewable);
    controller.set('jobstatus', model.jobstatus);
    controller.set('schid', model.id);

    this._super(controller, model);
  }
});

exports["default"] = _default;