"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _rsvp = require("rsvp");

var _service = require("@ember/service");

var _jquery = _interopRequireDefault(require("jquery"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  store: (0, _service.inject)(),
  urls: (0, _service.inject)("service"),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem('token') === "undefined") {
      window.location = this.get("urls.signout");
    }
  },
  queryParams: {
    page: {
      refreshModel: true
    },
    size: {
      refreshModel: true
    }
  },
  model: function model(params) {
    var store, hurl, data;
    return regeneratorRuntime.async(function model$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            store = this.get('store');
            hurl = this.get("urls.url") + 'paralog';
            _context.next = 4;
            return regeneratorRuntime.awrap(_jquery["default"].ajax({
              method: "POST",
              url: hurl,
              data: {
                'time': params.timeline
              }
            }).then((0, _runloop.bind)(this, function (data) {
              return data["result"];
            })));

          case 4:
            data = _context.sent;
            return _context.abrupt("return", (0, _rsvp.hash)({
              predictionlogs: data
            }));

          case 6:
          case "end":
            return _context.stop();
        }
      }
    }, null, this);
  },
  setupController: function setupController(controller, model) {
    controller.set('predictionlogs', model.predictionlogs);

    this._super(controller, model);
  }
});

exports["default"] = _default;