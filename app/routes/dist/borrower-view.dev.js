"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _route = _interopRequireDefault(require("@ember/routing/route"));

var _rsvp = require("rsvp");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _route["default"].extend({
  model: function model(params) {
    var store = this.get('store');
    return (0, _rsvp.hash)({
      borrower: store.find('borrower', params.borrower_id)
    });
  },
  setupController: function setupController(controller, model) {
    controller.set('borrower', model.borrower);

    this._super(controller, model);
  }
});

exports["default"] = _default;