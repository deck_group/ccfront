import Route from '@ember/routing/route';
import { hash } from 'rsvp';
import { inject as service } from '@ember/service';

export default Route.extend({
    store: service(),
    urls: service("service"),
    init() {
        this._super(...arguments)
        if (localStorage.getItem('token') === "undefined") {
            window.location = this.get("urls.signout");
        }
    },
    queryParams: {
        page: {
            refreshModel: true
        },
        size: {
            refreshModel: true
        }
    },

    model(params) {
        const store = this.get('store');
        return hash({
            data: store.query('predictionlog', {
                page: {
                    number: params.page,
                    size: params.size

                }
            })
        })

    },
    setupController(controller, model) {
        controller.set('predictionlogs', model.data);
        this._super(controller, model);
    }
});
