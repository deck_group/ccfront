"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _router = _interopRequireDefault(require("@ember/routing/router"));

var _environment = _interopRequireDefault(require("./config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Router = _router["default"].extend({
  location: _environment["default"].locationType,
  rootURL: _environment["default"].rootURL
});

Router.map(function () {
  this.route('scheduler');
  this.route('home');
  this.route('login', {
    path: '/'
  });
  this.route('scheduler-new', {
    path: '/scheduler/new'
  });
  this.route('scheduler-edit', {
    path: '/scheduler/:sche_id/edit'
  });
  this.route('scheduler-view', {
    path: '/scheduler/:sche_id/view'
  });
  this.route('logdetail');
  this.route('logdetail-view', {
    path: '/logdetail/:log_id/view'
  });
  this.route('borrower');
  this.route('borrower-view', {
    path: '/borrower/:borrowerid/view'
  });
  this.route('predictionreport', {
    path: '/prediction_report'
  });
  this.route('predictionreport-param', {
    path: '/prediction_report/:loantype/:province/:district'
  });
  this.route('homepage');
  this.route('prediction_batch', {
    path: '/prediction/batch'
  });
  this.route('borrower-params', {
    path: '/borrower/:borrowersearch'
  });
  this.route('prediction-log');
  this.route('predictionlog-params', {
    path: '/param/:timeline'
  });
});
var _default = Router;
exports["default"] = _default;