"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Model = _emberData["default"].Model,
    attr = _emberData["default"].attr;

var _default = Model.extend({
  borrower: attr('string'),
  contract: attr('string'),
  amount: attr('string'),
  rentalno: attr('string'),
  preddate: attr('string'),
  predthirty: attr('string'),
  predsixty: attr('string'),
  predninty: attr('string'),
  loantype: attr('string'),
  loanname: attr('string'),
  province: attr('string'),
  district: attr('string'),
  loanperiod: attr('string')
});

exports["default"] = _default;