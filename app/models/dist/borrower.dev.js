"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _emberData = _interopRequireDefault(require("ember-data"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var Model = _emberData["default"].Model,
    attr = _emberData["default"].attr;

var _default = Model.extend({
  borrowername: attr('string'),
  borrowerid: attr('string'),
  img: attr('string'),
  risklevel: attr('string'),
  predidate: attr('string'),
  mobileno: attr('string'),
  predata: attr('array'),
  age: attr('string'),
  gender: attr('string'),
  district: attr('string'),
  dob: attr('string'),
  province: attr('string'),
  addresslinethree: attr('string'),
  maritalstatus: attr('string'),
  contract: attr('array'),
  crm: attr('array'),
  contractno: attr('string'),
  scheduler: attr('array'),
  billgeneration: attr('array'),
  paymentbill: attr('array')
}); // sn = models.CharField(max_length=300)
// currentrental = models.IntegerField()
// preditime = models.CharField(max_length=300)
// risk = models.CharField(max_length=300)
// npl = models.CharField(max_length=300)
// confi =models.CharField(max_length=300)


exports["default"] = _default;