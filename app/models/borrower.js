import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    borrowerid: attr('string'),
    gender: attr('string'),
    district: attr('string'),
    age: attr('string'),
    addresslinethree:attr('string'),
    maritalstatus: attr('string'),
    province: attr('string')

});

