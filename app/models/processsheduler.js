import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    jobname: attr('string'),
    process: attr('string'),
    isweekly: attr('string'),
    ismonthly: attr('string'),
    isdaily: attr('string'),
    ishourly: attr('string'),
    month: attr('string'),
    monthday: attr('string'),
    weekday: attr('string'),
    hour: attr('string'),
    minute: attr('string'),
    second: attr('string'),
    status: attr('string'),
    dayofweek:attr('string')
});
