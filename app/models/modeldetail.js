import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    model: attr('string'),
    modelfilename: attr('string'),
    accuracy: attr('number'),
    precision: attr('number'),
    fone: attr('number'),
    recall: attr('number'),
    fpr: attr('number'),
    runtime: attr('string')
});
