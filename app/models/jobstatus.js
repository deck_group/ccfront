import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    jobid: attr('string'),
    status: attr('string')
});
