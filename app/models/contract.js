import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    borrowerid: attr('string'),
    contractno: attr('string'),
    intrate: attr('number'),
    loanname: attr('string'),
    loantype: attr('string'),
    mortagetype: attr('string'),
    period: attr('number'),
    principalamt: attr('number'),
    dateofissue: attr('string'),
    noofarrears: attr('number'),
});

