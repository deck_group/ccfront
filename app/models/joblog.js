import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    runid:attr('string'),
    jobid: attr('string'),
    name:attr('string'),
    dt: attr('string'),
    status: attr('string'),
    endtime: attr('string'),
    time: attr('string'),
    processname: attr('string'),
    output: attr('array')
});
