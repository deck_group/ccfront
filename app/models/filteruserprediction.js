import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    date: attr('string'),
    borrowerid: attr('string'),
    contractno: attr('string'),
    billdate: attr('string'),
    loanname: attr('string'),
    loantype: attr('string'),
    gender: attr('string'),
    provincename: attr('string'),
    districtname: attr('string'),
    rentalno: attr('number'),
    emi: attr('number'),
    principalamt: attr('number'),
    intrate: attr('number'),
    age: attr('number'),
    totpayable: attr('number'),
    maritialstatus: attr('string'),
    prediction: attr('number'),
    probability: attr('number'),
    username: attr('string'),
    mortagetype: attr('string'),
    timetake: attr('string'),
    model:attr('string'),
    status:attr('number'),
});

