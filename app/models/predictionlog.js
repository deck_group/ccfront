import DS from 'ember-data';
const {
    Model,
    attr
} = DS;

export default Model.extend({
    date: attr('string'),
    datetime: attr('string'),
    username: attr('string'),
    billdatefrom: attr('string'),
    billdateto: attr('string'),
    modelused: attr('string'),
});
