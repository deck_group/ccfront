import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `tr`,
    keyaccess: `name`,
    bus: service("pubsub"),
    store: service(),
    classTdOne: `col-sm-7 o_td_label`,
    classLabel: `o_form_label`,
    classTdTwo: `col-sm-5 padding-left`,
    classSelection: `form-control required`,
    selectionid: `ObjectId`,
    label: `Object Label`,
    broadcast: false,
    mode: ``,
    model: computed(function () {
        return this.get('store').findAll(this.get('modelname'))
    }),

    actions: {
        selectedOption(value) {
            this.set('setValue', value)
            if (this.get("broadcast") === true) {
                this.get('bus').interestTypepublish(value);

            }
        }
    }

});
