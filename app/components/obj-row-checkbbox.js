import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `td`,
    bus: service("pubsub"),
    isReadonly: false,
    spanClass: `bigcheck`,
    spanLabelClass: `bigcheck`,
    inputClass: `bigcheck`,
    spanTargetClass: `bigcheck-target`,
    broadcast: false,
    objname: ``,
    actions: {
        optionsChecked() {
            var flag=document.getElementById(this.get("objname")).checked
            this.set('data',flag)
            if (this.get("broadcast") === true) {
                this.get('bus').flagPublish(this.get("objname"), flag);
            }
        }
    }
});
