import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    tagName: `div`,
    keyaccess: `name`,
    store: service(),
    classdivOne: `col-sm-2 padding-right`,
    classLabel:`o_form_label_two`,
    classdivTwo: `col-sm-10 padding-left_5`,
    classSelection: `form-control required_one`,
    selectionid: `ObjectId`,
    label: `Object Label`,
    broadcast: false,
    mode: ``,
    data:``,
    classNames:[`col-sm-12`],
    model: computed(function () {
        return this.get('store').findAll(this.get('modelname'))
    }),

    actions: {
        selectedOption(value) {
            this.set('data', value)
        }
    }
});
