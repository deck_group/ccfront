import Component from '@ember/component';

export default Component.extend({
    model:``,
    tagName:`div`,
    classNames:[`markdown-converter__text--rendered`],
    classOne:`col-sm-2`,
    classOneNew:`col-sm-7 border`,
    classTitle:`col-sm-6 header`,
    classBody:`col-sm-6 p`,
    classRow:`row`
});
