import Component from '@ember/component';

export default Component.extend({
    tagName:`div`,
    classNames:['sc-qXFrf cKRnEE'],
    label:``,
    symbol:``,
    classIcon:`rmwc-icon google-material-icons sc-AxgMl eRwiFs sc-psrQp fATUev`,
    classOne:`sc-fzqARJ sc-fzqNqU sc-qPJtC FTRCN`,
    classTwo:`sc-oUqyN hruVos`,

});
