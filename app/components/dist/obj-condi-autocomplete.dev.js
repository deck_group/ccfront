"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

var _object = require("@ember/object");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  classNames: ["row block-link block-link--bordered"],
  tagName: 'div',
  label: "Object Title",
  classOne: "kernels-teaser__item",
  classTwo: "kernels-teaser__item-title",
  inputClass: "form-control required",
  classTwo_r: "col-sm-2 gfir",
  classTwo_b: "col-sm-7",
  lists: "",
  keyaccess: "",
  store: (0, _service.inject)(),
  data: "",
  modeldata: undefined,
  bus: (0, _service.inject)("pubsub"),
  model: (0, _object.computed)('modeldata', function () {
    return this.get('modeldata');
  }),
  init: function init() {
    this._super.apply(this, arguments);

    this.get('bus').on('conditional', this, this.recieve);
  },
  recieve: function recieve(data) {
    this.set('modeldata', data);
    this.get('model');
  },
  actions: {
    selectedOption: function selectedOption(data) {
      console.log(data);
    }
  }
});

exports["default"] = _default;