"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "div",
  classone: "sc-jCCNBi dnDHOZTT",
  classtwo: "sc-jNfLxA gjvUWV",
  classthree: "sc-kIFxrv bzKand",
  classspan: "sc-fzqAbL sc-fzqMAW dmEdIJ",
  classfour: "sc-jCmwxO sc-kNYnxC fbAYtQ",
  clasNames: ["col-sm-12"],
  classnumber: "number"
});

exports["default"] = _default;