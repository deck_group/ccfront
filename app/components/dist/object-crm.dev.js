"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  model: "",
  tagName: "div",
  classNames: ["markdown-converter__text--rendered"],
  classOne: "col-sm-2",
  classOneNew: "col-sm-7 border",
  classTitle: "col-sm-6 header",
  classBody: "col-sm-6 p",
  classRow: "row"
});

exports["default"] = _default;