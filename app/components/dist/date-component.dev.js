"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "div",
  classdivOne: "col-sm-2 padding-right",
  classLabel: "o_form_label_two",
  classdivTwo: "col-sm-10 padding-left_5",
  classSelection: "form-control requiredll",
  selectionid: "ObjectId",
  label: "Object Label",
  broadcast: false,
  mode: "",
  data: "",
  classNames: ["col-sm-12"]
});

exports["default"] = _default;