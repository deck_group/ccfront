"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "div",
  classNames: ['sc-qXFrf cKRnEE'],
  label: "",
  symbol: "",
  classIcon: "rmwc-icon google-material-icons sc-AxgMl eRwiFs sc-psrQp fATUev",
  classOne: "sc-fzqARJ sc-fzqNqU sc-qPJtC FTRCN",
  classTwo: "sc-oUqyN hruVos"
});

exports["default"] = _default;