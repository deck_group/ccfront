"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  classNames: ["row block-link block-link--bordered"],
  tagName: 'div',
  label: "Object Title",
  classOne: "kernels-teaser__item",
  classTwo: "kernels-teaser__item-title",
  inputClass: "form-control required",
  classTwo_r: "col-sm-2 gfir",
  classTwo_b: "col-sm-9",
  lists: "",
  keyaccess: "",
  store: (0, _service.inject)(),
  data: "",
  modeldata: "",
  bus: (0, _service.inject)("pubsub"),
  broadcast: "false",
  model: (0, _object.computed)(function () {
    var data = this.get('store').findAll(this.get('modelname'));
    this.set('modeldata', data);
    return data;
  }),
  actions: {
    selectedOption: function selectedOption(data) {
      var that = this;
      this.get('modeldata').then(function (model) {
        model.forEach(function (story) {
          var key = that.get("keyaccess");

          if (story[key] === data) {
            that.get('bus').conditionalPublish(story.loanname);
          }
        });
      });
    }
  }
});

exports["default"] = _default;