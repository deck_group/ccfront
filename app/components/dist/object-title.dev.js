"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _objectTitleValidator = _interopRequireDefault(require("../mixins/object-title-validator"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend(_objectTitleValidator["default"], {
  isReadonly: false,
  message: undefined,
  format: "^[a-zA-Z0-9\\s.@-_,']*$",
  isPresence: true,
  leasdingspace: true,
  tagName: 'div',
  classNames: ["row block-link block-link--bordered"],
  title: undefined,
  placeholder: "",
  label: "Object Title",
  classOne: "kernels-teaser__item",
  classTwo: "kernels-teaser__item-title",
  inputClass: "form-control required",
  minlength: 5,
  maxlength: 4096,
  setNameError: false,
  actions: {
    clearMessage: function clearMessage() {
      this.set('message', undefined);
    },
    checkTitle: function checkTitle() {}
  }
});

exports["default"] = _default;