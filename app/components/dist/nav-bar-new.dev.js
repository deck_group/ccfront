"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _service = require("@ember/service");

var _object = require("@ember/object");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  store: (0, _service.inject)(),
  router: (0, _service.inject)(),
  tagName: "div",
  classNames: ["sc-pCPhY sc-pbMuv bHDFEb"],
  classOne: "sc-pkgFA kIrMbp",
  classTwo: "sc-ptBBy kpuSv",
  classThree: "sc-qPzgd TZTEY",
  classBTN: "sc-fznAgC keRlOj google-material-icons",
  classFour: "sc-qXUgY gsTPqK",
  classImage: "sc-plgA-D dNfjzh",
  classFive: "sc-pRDlx dHYhnw",
  classSix: "km-list",
  classSeven: "sc-puCJS iyLynJ",
  classA: "sc-puCJS iyLynJ",
  classHome: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classSch: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classPredRep: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classPredBatch: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classBorrow: "sc-pRtAn kSauRZ sc-qWSYE kIwrRP",
  classIcon: "rmwc-icon google-material-icons sc-AxheI dsqNyx",
  sizevalue: "24px",
  classBarHome: "",
  classBarSch: "",
  classBarBorrow: "",
  classBarPre: "",
  classBarMatchPre: "",
  classHomeCompute: (0, _object.computed)('classHome', function () {
    return this.get('classHome');
  }),
  classSchCompute: (0, _object.computed)('classSch', function () {
    return this.get('classSch');
  }),
  classBarHomeCompute: (0, _object.computed)('classBarHome', function () {
    return this.get('classBarHome');
  }),
  classBarSchCompute: (0, _object.computed)('classBarSch', function () {
    return this.get('classBarSch');
  }),
  classBarPreCompute: (0, _object.computed)('classBarPre', function () {
    return this.get('classBarPre');
  }),
  classBarBorrowCompute: (0, _object.computed)('classBarBorrow', function () {
    return this.get('classBarBorrow');
  }),
  init: function init() {
    this._super.apply(this, arguments);

    if (localStorage.getItem('item') !== undefined) {
      if (localStorage.getItem('item') === 'home') {
        this.set('classHome', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP');
        this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBarHome', 'sc-qOxXJ enJAMb');
        this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBarPre', '');
        this.set('classBarSch', '');
        this.set('classBarBorrow', '');
        this.set('classBarMatchPre', '');
      } else if (localStorage.getItem('item') === 'jobsch') {
        this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classSch', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP');
        this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBarPre', '');
        this.set('classBarHome', '');
        this.set('classBarSch', 'sc-qOxXJ enJAMb');
        this.set('classBarBorrow', '');
        this.set('classBarMatchPre', '');
      } else if (localStorage.getItem('item') === 'borrower') {
        this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBorrow', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP');
        this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBarPre', '');
        this.set('classBarHome', '');
        this.set('classBarSch', '');
        this.set('classBarBorrow', 'sc-qOxXJ enJAMb');
        this.set('classBarMatchPre', '');
      } else if (localStorage.getItem('item') === 'predicReport') {
        this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredRep', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP');
        this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBarHome', '');
        this.set('classBarSch', '');
        this.set('classBarBorrow', '');
        this.set('classBarPre', 'sc-qOxXJ enJAMb');
        this.set('classBarMatchPre', '');
      } else if (localStorage.getItem('item') === 'predicbatch') {
        this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP');
        this.set('classPredBatch', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP');
        this.set('classBarHome', '');
        this.set('classBarSch', '');
        this.set('classBarBorrow', '');
        this.set('classBarPre', '');
        this.set('classBarMatchPre', 'sc-qOxXJ enJAMb');
      }
    }
  },
  actions: {
    home: function home() {
      this.get('router').transitionTo('home');
    },
    homeone: function homeone() {
      localStorage.setItem('item', undefined);
      localStorage.setItem('item', 'home');
      this.get('router').transitionTo('home');
    },
    schedule: function schedule() {
      localStorage.setItem('item', undefined);
      localStorage.setItem('item', 'jobsch');
      this.get('router').transitionTo('scheduler');
    },
    borrower: function borrower() {
      localStorage.setItem('item', undefined);
      localStorage.setItem('item', 'borrower');
      this.get('router').transitionTo('borrower');
    },
    predicReport: function predicReport() {
      localStorage.setItem('item', undefined);
      localStorage.setItem('item', 'predicReport');
      this.get('router').transitionTo('predictionreport');
    },
    predicBatch: function predicBatch() {
      localStorage.setItem('item', undefined);
      localStorage.setItem('item', 'predicbatch');
      this.get('router').transitionTo('prediction_batch');
    }
  }
});

exports["default"] = _default;