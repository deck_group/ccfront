"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "section",
  queryParams: ['paginateBy'],
  page: 1,
  paginateBy: 1,
  paginatedItems: computed('items', 'page', function () {
    var i = (parseInt(this.get('page')) - 1) * parseInt(this.get('paginateBy'));
    var j = i + parseInt(this.get('paginateBy'));
    return this.get('items').slice(i, j);
  }),
  numberOfPages: computed('page', function () {
    var n = this.get('items.length');
    var c = parseInt(this.get('paginateBy'));
    var r = Math.floor(n / c);

    if (n % c > 0) {
      r += 1;
    }

    return r;
  }),
  pageNumbers: computed('numberOfPages', function () {
    var n = Array(this.get('numberOfPages'));

    for (var i = 0; i < n.length; i++) {
      n[i] = i + 1;
    }

    return n;
  }),
  showNext: computed('page', function () {
    return this.get('page') < this.get('numberOfPages');
  }),
  showPrevious: Ember.computed('page', function () {
    return this.get('page') > 1;
  }),
  nextText: 'Next',
  previousText: 'Previous',
  actions: {
    more: function more(searchtext, lastid) {
      this.get("router").transitionTo('searchresult', searchtext, lastid);
    },
    nextClicked: function nextClicked() {
      if (this.get('page') + 1 <= this.get('numberOfPages')) {
        this.set('page', this.get('page') + 1);
      }
    },
    previousClicked: function previousClicked() {
      if (this.get('page') > 0) {
        this.set('page', this.get('page') - 1);
      }
    },
    pageClicked: function pageClicked(pageNumber) {
      this.set('page', pageNumber);
    }
  }
});

exports["default"] = _default;