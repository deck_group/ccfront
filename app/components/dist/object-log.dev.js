"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "span",
  isReadonly: false,
  mode: "",
  className: ["sc-fzqAbL sc-fzqMAW dmEdIJ"],
  classOne: "sc-jCmwxO sc-kNYnxC fbAYtQTwo"
});

exports["default"] = _default;