"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _component = _interopRequireDefault(require("@ember/component"));

var _object = require("@ember/object");

var _service = require("@ember/service");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _component["default"].extend({
  tagName: "tr",
  keyaccess: "name",
  bus: (0, _service.inject)("pubsub"),
  store: (0, _service.inject)(),
  classTdOne: "col-sm-7 o_td_label",
  classLabel: "o_form_label",
  classTdTwo: "col-sm-5 padding-left",
  classSelection: "form-control required",
  selectionid: "ObjectId",
  label: "Object Label",
  broadcast: false,
  mode: "",
  model: (0, _object.computed)(function () {
    return this.get('store').findAll(this.get('modelname'));
  }),
  actions: {
    selectedOption: function selectedOption(value) {
      this.set('setValue', value);

      if (this.get("broadcast") === true) {
        this.get('bus').interestTypepublish(value);
      }
    }
  }
});

exports["default"] = _default;