import Component from '@ember/component';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Component.extend({
    classNames: [`row block-link block-link--bordered`],
    tagName: 'div',
    label: `Object Title`,
    classOne: `kernels-teaser__item`,
    classTwo: `kernels-teaser__item-title`,
    inputClass: `form-control required`,
    classTwo_r: `col-sm-2 gfir`,
    classTwo_b: `col-sm-9`,
    lists: ``,
    keyaccess: ``,
    store: service(),
    data: ``,
    modeldata: ``,
    bus: service("pubsub"),
    broadcast:"false",
    model: computed(function () {
        var data = this.get('store').findAll(this.get('modelname'))
        this.set('modeldata', data)
        return data
    }),
    actions: {
        selectedOption(data) {
            var that=this
            this.get('modeldata').then(function (model) {
                model.forEach(function (story) {
                    var key=that.get("keyaccess")
                    if(story[key]===data){
                        that.get('bus').conditionalPublish(story.loanname);
                    }
                });
            });
        }
    }
});
