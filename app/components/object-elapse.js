import Component from '@ember/component';

export default Component.extend({
    tagName:`tr`,
    classone:`col-sm-6`,
    labelClass: `o_form_label`,
});
