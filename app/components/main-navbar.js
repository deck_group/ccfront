import Component from '@ember/component';
import { inject as service } from '@ember/service';

export default Component.extend({
    router: service(),
    allpub:service('puballlog'),
    loginpub:service('logpublogin'),
    actions: {
        logout() {
           
            var dict={}
            dict['userid']=localStorage.getItem('user')
            dict['username']=localStorage.getItem('username')
            dict['userrole']=localStorage.getItem('role')
            dict['activitycat']="logout"
            dict['activitysubcat']="N/A"
            dict['status']="success"
            dict['statuscode']=200
            dict['variablebefore']={'data':'No data'}
            dict['variableafter']={'data':'No data'}
            dict['comment']='N/A'
            dict['reason']='success'
            dict['reasoncode']=200
            this.get("allpub").alllog(dict)

            var dicttow={}
            dicttow['userid']=localStorage.getItem('user')
            dicttow['username']=localStorage.getItem('username')
            dicttow['userrole']=localStorage.getItem('role')
            dicttow['activitytype']="logout"
            dicttow['inputuserid']="N/A"
            dicttow['inputpassword']="N/A"
            dicttow['lat']='N/A'
            dicttow['lng']='N/A'
            dicttow['ip']='N/A'
            dicttow['status']="success"
            dicttow['statuscode']=200
            dicttow['variablebefore']={'data':'No data'}
            dicttow['variableafter']={'data':'No data'}
            dicttow['comment']='N/A'
            dicttow['reason']='success'
            dicttow['reasoncode']=200
            this.get("loginpub").loginpub(dicttow)

            localStorage.setItem("token", undefined)
            localStorage.setItem("user", undefined)
            localStorage.setItem('username', undefined)
            localStorage.setItem('role', undefined)
            localStorage.setItem('password', undefined)


            this.get('router').transitionTo('login')
        }
    }

});
