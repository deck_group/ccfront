import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
    formclass: `form-group`,
    highlightclass: `col-sm-12 no-highlight`,
    rowclass: `row`,
    colclass: `col-sm-3`,
    colclasstwo: `col-sm-6`,
    btnclass: `btn btn-default`,
    btnLabel: `Sign In`,
    message: undefined,
    store: service(),
    router: service(),
    allpub: service('puballlog'),
    loginpub: service('logpublogin'),
    newmodel: undefined,
    messageArray: computed('message', function () {
        return this.get('message')
    }),
    actions: {
        submit(newmodel) {
            this.set('message', undefined)
            var that = this
            if (Object.keys(newmodel).length === 0) {
                localStorage.setItem('token', undefined)

            }
            else {
                this.get('store').query('logauth', {
                    query: {
                        value1: newmodel.loginid,
                        value: newmodel.password
                    }
                }).then(
                    function (model) {
                        if (model.content.length === 0) {
                            that.set("message", ["Please check the Login ID or Password"])
                            that.get('messageArray')
                            var dict = {}
                            dict['userid'] = newmodel.loginid
                            dict['username'] = "N/A"
                            dict['userrole'] = "N/A"
                            dict['activitycat'] = "login"
                            dict['activitysubcat'] = "N/A"
                            dict['status'] = "failed"
                            dict['statuscode'] = 400
                            dict['variablebefore'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                            dict['variableafter'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                            dict['comment'] = 'N/A'
                            dict['reason'] = 'Authentication failed'
                            dict['reasoncode'] = 300
                            that.get("allpub").alllog(dict)

                            var dicttow = {}
                            dicttow['userid'] = newmodel.loginid
                            dicttow['username'] = "N/A"
                            dicttow['userrole'] = "N/A"
                            dicttow['activitytype'] = "login"
                            dicttow['inputuserid'] = newmodel.loginid
                            dicttow['inputpassword'] = newmodel.password
                            dicttow['lat'] = 'N/A'
                            dicttow['lng'] = 'N/A'
                            dicttow['ip'] = 'N/A'
                            dicttow['status'] = "failed"
                            dicttow['statuscode'] = 400
                            dicttow['variablebefore'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                            dicttow['variableafter'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                            dicttow['comment'] = 'N/A'
                            dicttow['reason'] = 'Authentication failed'
                            dicttow['reasoncode'] = 300
                            that.get("loginpub").loginpub(dicttow)
                        }
                        else {
                            model.forEach(function (data) {
                                that.set('newmodel', {})
                                localStorage.setItem('token', data.token)
                                localStorage.setItem('user', data.loginid)
                                localStorage.setItem('username', data.name)
                                localStorage.setItem('password', data.password)
                                localStorage.setItem('role', data.role)
                                var dict = {}
                                dict['userid'] = newmodel.loginid
                                dict['username'] = data.name
                                dict['userrole'] = data.role
                                dict['activitycat'] = "login"
                                dict['activitysubcat'] = "N/A"
                                dict['status'] = "success"
                                dict['statuscode'] = 200
                                dict['variablebefore'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                                dict['variableafter'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                                dict['comment'] = 'N/A'
                                dict['reason'] = 'success'
                                dict['reasoncode'] = 200
                                that.get("allpub").alllog(dict)

                                var dicttow = {}
                                dicttow['userid'] = newmodel.loginid
                                dicttow['username'] = data.name
                                dicttow['userrole'] = data.role
                                dicttow['activitytype'] = "login"
                                dicttow['inputuserid'] = newmodel.loginid
                                dicttow['inputpassword'] = newmodel.password
                                dicttow['lat'] = 'N/A'
                                dicttow['lng'] = 'N/A'
                                dicttow['ip'] = 'N/A'
                                dicttow['status'] = "success"
                                dicttow['statuscode'] = 200
                                dicttow['variablebefore'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                                dicttow['variableafter'] = { 'loginid': newmodel.loginid, 'password': newmodel.password }
                                dicttow['comment'] = 'N/A'
                                dicttow['reason'] = 'success'
                                dicttow['reasoncode'] = 200 
                                that.get("loginpub").loginpub(dicttow)
                                that.get('router').transitionTo('home')
                            });
                        }

                    }
                )


            }
        },

    }
});
