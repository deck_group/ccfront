import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Component.extend({
    store: service(),
    router: service(),
    tagName: `div`,
    classNames: [`sc-pCPhY sc-pbMuv bHDFEb`],
    classOne: `sc-pkgFA kIrMbp`,
    classTwo: `sc-ptBBy kpuSv`,
    classThree: `sc-qPzgd TZTEY`,
    classBTN: `sc-fznAgC keRlOj google-material-icons`,
    classFour: `sc-qXUgY gsTPqK`,
    classImage: `sc-plgA-D dNfjzh`,
    classFive: `sc-pRDlx dHYhnw`,
    classSix: `km-list`,
    classSeven: `sc-puCJS iyLynJ`,
    classA: `sc-puCJS iyLynJ`,
    classHome: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classSch: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classPredRep: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classPredBatch: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,

    classBorrow: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classIcon: `rmwc-icon google-material-icons sc-AxheI dsqNyx`,
    sizevalue: `24px`,
    classBarHome: ``,
    classBarSch: ``,
    classBarBorrow: ``,
    classBarPre: ``,
    classBarMatchPre: ``,
    classPredLog: `sc-pRtAn kSauRZ sc-qWSYE kIwrRP`,
    classBarPredPre: ``,
    classHomeCompute: computed('classHome', function () {
        return this.get('classHome')
    }),
    classSchCompute: computed('classSch', function () {
        return this.get('classSch')
    }),

    expandmention: `expand_more`,
    expandmentionshow: "false",

    classBarHomeCompute: computed('classBarHome', function () {
        return this.get('classBarHome')
    }),
    classBarSchCompute: computed('classBarSch', function () {
        return this.get('classBarSch')
    }),
    classBarPreCompute: computed('classBarPre', function () {
        return this.get('classBarPre')
    }),
    classBarBorrowCompute: computed('classBarBorrow', function () {
        return this.get('classBarBorrow')
    }),

    init() {
        this._super(...arguments);
        if (localStorage.getItem('item') !== undefined) {
            if (localStorage.getItem('item') === 'home') {
                this.set('classHome', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarHome', 'sc-qOxXJ enJAMb')
                this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredLog', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarPredPre', '')
                this.set('classBarPre', '')
                this.set('classBarSch', '')
                this.set('classBarBorrow', '')
                this.set('classBarMatchPre', '')
            } else if (localStorage.getItem('item') === 'jobsch') {
                this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredLog', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarPredPre', '')
                this.set('classBarPre', '')
                this.set('classBarHome', '')
                this.set('classBarSch', 'sc-qOxXJ enJAMb')
                this.set('classBarBorrow', '')
                this.set('classBarMatchPre', '')
            } else if (localStorage.getItem('item') === 'borrower') {
                this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP')
                this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredLog', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarPredPre', '')
                this.set('classBarPre', '')
                this.set('classBarHome', '')
                this.set('classBarSch', '')
                this.set('classBarBorrow', 'sc-qOxXJ enJAMb')
                this.set('classBarMatchPre', '')

            } else if (localStorage.getItem('item') === 'predicReport') {
                this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredRep', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredLog', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarPredPre', '')
                this.set('classBarHome', '')
                this.set('classBarSch', '')
                this.set('classBarBorrow', '')
                this.set('classBarPre', 'sc-qOxXJ enJAMb')
                this.set('classBarMatchPre', '')

            }
            else if (localStorage.getItem('item') === 'predicbatch') {
                this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn hLEAFZ sc-qWSYE kIwrRP')
                this.set('classPredLog', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBarPredPre', '')
                this.set('classBarHome', '')
                this.set('classBarSch', '')
                this.set('classBarBorrow', '')
                this.set('classBarPre', '')
                this.set('classBarMatchPre', 'sc-qOxXJ enJAMb')

            }
            else if (localStorage.getItem('item') === 'predilog') {
                this.set('classHome', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classSch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classBorrow', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredRep', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredBatch', 'sc-pRtAn kSauRZ sc-qWSYE kIwrRP')
                this.set('classPredLog', 'sc-pRtAn hLEAFZ  sc-qWSYE kIwrRP')
                this.set('classBarPredPre', 'sc-qOxXJ enJAMb')
                this.set('classBarHome', '')
                this.set('classBarSch', '')
                this.set('classBarBorrow', '')
                this.set('classBarPre', '')
                this.set('classBarMatchPre', '')
            }
        }
    },
    actions: {
        batchlog(expand) {
            if (expand === "expand_more") {
                this.set("expandmention", `expand_less`)
                this.set("expandmentionshow", "true")
            }
            else {
                this.set("expandmention", `expand_more`)
                this.set("expandmentionshow", "false")
            }
        },
        home() {
            this.get('router').transitionTo('home')
        },

        homeone() {
            localStorage.setItem('item', undefined)
            localStorage.setItem('item', 'home')
            this.set("expandmention", `expand_more`)
            this.set("expandmentionshow", "false")
            this.get('router').transitionTo('home')
        },
        schedule() {
            localStorage.setItem('item', undefined)
            localStorage.setItem('item', 'jobsch')
            this.set("expandmention", `expand_more`)
            this.set("expandmentionshow", "false")
            this.get('router').transitionTo('scheduler')
        },
        borrower() {
            localStorage.setItem('item', undefined)
            localStorage.setItem('item', 'borrower')
            this.set("expandmention", `expand_more`)
            this.set("expandmentionshow", "false")
            this.get('router').transitionTo('borrower')
        },
        predicReport() {
            localStorage.setItem('item', undefined)
            localStorage.setItem('item', 'predicReport')
            this.set("expandmention", `expand_more`)
            this.set("expandmentionshow", "false")
            this.get('router').transitionTo('predictionreport')
        },
        predicBatch() {
            localStorage.setItem('item', undefined)
            localStorage.setItem('item', 'predicbatch')
            this.set("expandmention", `expand_less`)
            this.set("expandmentionshow", "true")
            this.get('router').transitionTo('prediction_batch')

        },

        predictionlog() { 
            localStorage.setItem('item', undefined)
            localStorage.setItem('item', 'predilog')
            this.set("expandmention", `expand_less`)
            this.set("expandmentionshow", "true")
            this.get('router').transitionTo('prediction-log')
            
        }
    }
});
