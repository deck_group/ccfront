import Component from '@ember/component';

export default Component.extend({
    tagName: `div`,
    classdivOne: `col-sm-2 padding-right`,
    classLabel: `o_form_label_two`,
    classdivTwo: `col-sm-10 padding-left_5`,
    classSelection: `form-control requiredll`,
    selectionid: `ObjectId`,
    label: `Object Label`,
    broadcast: false,
    mode: ``,
    data:``,
    classNames:[`col-sm-12`]

});
