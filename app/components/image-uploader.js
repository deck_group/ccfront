import Component from '@ember/component';

export default Component.extend({
    tagName: `div`,
    classRow: `row`,
    src: `/assets/images/user.png`,
    classImage: `asset-image-preview`,
});
