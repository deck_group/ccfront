    import Controller from '@ember/controller';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    month:undefined,
    year:undefined,
    actions:{
        filter(month,year){
            var fl=year+''+month
            this.set('year',undefined)
            this.set('month',undefined)
            this.transitionToRoute('predictionlog-params',fl)
        }
    }
});
