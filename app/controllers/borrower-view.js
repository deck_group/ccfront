import Controller from '@ember/controller';
import { computed } from '@ember/object';
import { inject as service } from '@ember/service';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    store:service(),
    toShow:"true",
    contractno:undefined,
    toShowCompute: computed('toShow', function () {
        return this.get('toShow')
    }),
    predictionarray:undefined,
    predictionarrayCompute: computed('predictionarray', function () {
        return this.get('predictionarray')
    }),
    contractArray:undefined,
    contractArraycompute:computed('contractArray', function () {
        return this.get('contractArray')
    }),
    contractnocompute:computed('contractno', function () {
        return this.get('contractno')
    }),
    actions: {
        getdata(contractno){
            var data=this.get('store').query('filteruserprediction', {
                contract_no:contractno
            })

            var datacon=this.get('store').query('contract', {
                contract_no:contractno
            })

            this.set("contractno",contractno)
            this.get('contractnocompute')

            this.set('predictionarray',undefined)
            this.set('predictionarray',data)
            this.get('predictionarrayCompute')

            this.set('contractArray',undefined)
            this.set('contractArray',datacon)
            this.get('contractArraycompute')

            this.send("pretab")
            
        },
        discardroute() {
            this.set("contractno",undefined)
            this.set('predictionarray',undefined)
            this.set('contractArray',undefined)
            this.transitionToRoute("borrower")
        },
        pretab(){

            var contract = document.getElementById('notebook_page_90')
            contract.setAttribute("class", "tab-pane")

            // var crm = document.getElementById('notebook_page_91')
            // crm.setAttribute("class", "tab-pane")

            var contracticon = document.getElementById('contract')
            contracticon.setAttribute("class", "competition-overview__nav-item")

            // var crmicon = document.getElementById('crm')
            // crmicon.setAttribute("class", "competition-overview__nav-item")

            var scheduler = document.getElementById('notebook_page_92')
            scheduler.setAttribute("class", "tab-pane")

            var scheicon = document.getElementById('sche')
            scheicon.setAttribute("class", "competition-overview__nav-item")

            var billgen = document.getElementById('notebook_page_93')
            billgen.setAttribute("class", "tab-pane")

            var billicon = document.getElementById('bill')
            billicon.setAttribute("class", "competition-overview__nav-item")

            var paygen = document.getElementById('notebook_page_94')
            paygen.setAttribute("class", "tab-pane")

            var payicon = document.getElementById('payment')
            payicon.setAttribute("class", "competition-overview__nav-item")

            var predgen = document.getElementById('notebook_page_95')
            predgen.setAttribute("class", "active")

            var preicon = document.getElementById('pred')
            preicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected")
            
          


        },
        paytab(){
            var contract = document.getElementById('notebook_page_90')
            contract.setAttribute("class", "tab-pane")

            // var crm = document.getElementById('notebook_page_91')
            // crm.setAttribute("class", "tab-pane")

            var contracticon = document.getElementById('contract')
            contracticon.setAttribute("class", "competition-overview__nav-item")

            // var crmicon = document.getElementById('crm')
            // crmicon.setAttribute("class", "competition-overview__nav-item")

            var scheduler = document.getElementById('notebook_page_92')
            scheduler.setAttribute("class", "tab-pane")

            var scheicon = document.getElementById('sche')
            scheicon.setAttribute("class", "competition-overview__nav-item")

            var billgen = document.getElementById('notebook_page_93')
            billgen.setAttribute("class", "tab-pane")

            var billicon = document.getElementById('bill')
            billicon.setAttribute("class", "competition-overview__nav-item")

            var paygen = document.getElementById('notebook_page_94')
            paygen.setAttribute("class", "active")

            var payicon = document.getElementById('payment')
            payicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected")

            var predgen = document.getElementById('notebook_page_95')
            predgen.setAttribute("class", "tab-pane")

            var preicon = document.getElementById('pred')
            preicon.setAttribute("class", "competition-overview__nav-item")

        },
        billtab(){
            var contract = document.getElementById('notebook_page_90')
            contract.setAttribute("class", "tab-pane")

            // var crm = document.getElementById('notebook_page_91')
            // crm.setAttribute("class", "tab-pane")

            var contracticon = document.getElementById('contract')
            contracticon.setAttribute("class", "competition-overview__nav-item")

            // var crmicon = document.getElementById('crm')
            // crmicon.setAttribute("class", "competition-overview__nav-item")

            var scheduler = document.getElementById('notebook_page_92')
            scheduler.setAttribute("class", "tab-pane")

            var scheicon = document.getElementById('sche')
            scheicon.setAttribute("class", "competition-overview__nav-item")

            var billgen = document.getElementById('notebook_page_93')
            billgen.setAttribute("class", "active")

            var billicon = document.getElementById('bill')
            billicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected")

            var paygen = document.getElementById('notebook_page_94')
            paygen.setAttribute("class", "tab-pane")

            var payicon = document.getElementById('payment')
            payicon.setAttribute("class", "competition-overview__nav-item")
            
            var predgen = document.getElementById('notebook_page_95')
            predgen.setAttribute("class", "tab-pane")

            var preicon = document.getElementById('pred')
            preicon.setAttribute("class", "competition-overview__nav-item")



        },
        schetab(){

            var contract = document.getElementById('notebook_page_90')
            contract.setAttribute("class", "tab-pane")

            // var crm = document.getElementById('notebook_page_91')
            // crm.setAttribute("class", "tab-pane")

            var contracticon = document.getElementById('contract')
            contracticon.setAttribute("class", "competition-overview__nav-item")

            // var crmicon = document.getElementById('crm')
            // crmicon.setAttribute("class", "competition-overview__nav-item")

            var scheduler = document.getElementById('notebook_page_92')
            scheduler.setAttribute("class", "active")

            var scheicon = document.getElementById('sche')
            scheicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected")

            var billgen = document.getElementById('notebook_page_93')
            billgen.setAttribute("class", "tab-pane")

            var billicon = document.getElementById('bill')
            billicon.setAttribute("class", "competition-overview__nav-item")

            var paygen = document.getElementById('notebook_page_94')
            paygen.setAttribute("class", "tab-pane")

            var payicon = document.getElementById('payment')
            payicon.setAttribute("class", "competition-overview__nav-item")

            var predgen = document.getElementById('notebook_page_95')
            predgen.setAttribute("class", "tab-pane")

            var preicon = document.getElementById('pred')
            preicon.setAttribute("class", "competition-overview__nav-item")



        },
        contracttab() {

            var contract = document.getElementById('notebook_page_90')
            contract.setAttribute("class", "active")

            // var crm = document.getElementById('notebook_page_91')
            // crm.setAttribute("class", "tab-pane")

            var contracticon = document.getElementById('contract')
            contracticon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected")

            // var crmicon = document.getElementById('crm')
            // crmicon.setAttribute("class", "competition-overview__nav-item")

            var scheduler = document.getElementById('notebook_page_92')
            scheduler.setAttribute("class", "tab-pane")

            var scheicon = document.getElementById('sche')
            scheicon.setAttribute("class", "competition-overview__nav-item")

            var billgen = document.getElementById('notebook_page_93')
            billgen.setAttribute("class", "tab-pane")

            var billicon = document.getElementById('bill')
            billicon.setAttribute("class", "competition-overview__nav-item")

            var paygen = document.getElementById('notebook_page_94')
            paygen.setAttribute("class", "tab-pane")

            var payicon = document.getElementById('payment')
            payicon.setAttribute("class", "competition-overview__nav-item")

            var predgen = document.getElementById('notebook_page_95')
            predgen.setAttribute("class", "tab-pane")

            var preicon = document.getElementById('pred')
            preicon.setAttribute("class", "competition-overview__nav-item")

        },
        crmtab(){

            var contract = document.getElementById('notebook_page_90')
            contract.setAttribute("class", "tab-pane")

            var crm = document.getElementById('notebook_page_91')
            crm.setAttribute("class", "active")

            var scheduler = document.getElementById('notebook_page_92')
            scheduler.setAttribute("class", "tab-pane")

            var billgen = document.getElementById('notebook_page_93')
            billgen.setAttribute("class", "tab-pane")

            var billicon = document.getElementById('bill')
            billicon.setAttribute("class", "competition-overview__nav-item")

            var contracticon = document.getElementById('contract')
            contracticon.setAttribute("class", "competition-overview__nav-item")
            
            var crmicon = document.getElementById('crm')
            crmicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected")

            var scheicon = document.getElementById('sche')
            scheicon.setAttribute("class", "competition-overview__nav-item")

            var paygen = document.getElementById('notebook_page_94')
            paygen.setAttribute("class", "tab-pane")

            var payicon = document.getElementById('payment')
            payicon.setAttribute("class", "competition-overview__nav-item")

            var predgen = document.getElementById('notebook_page_95')
            predgen.setAttribute("class", "tab-pane")

            var preicon = document.getElementById('pred')
            preicon.setAttribute("class", "competition-overview__nav-item")




        }
        

    }
});
