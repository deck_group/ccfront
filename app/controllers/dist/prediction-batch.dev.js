"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _jquery = _interopRequireDefault(require("jquery"));

var _environment = _interopRequireDefault(require("../config/environment"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  modelname: "",
  start: "",
  end: "",
  actions: {
    discardroute: function discardroute() {},
    run: function run(modelname, start, end) {
      var that = this;
      var url = _environment["default"].API_OTHER_HOST + 'batchpredictions';
      var that = this;

      _jquery["default"].ajax({
        method: "POST",
        url: url,
        data: {
          start: start,
          end: end,
          model: modelname,
          user: localStorage.getItem("user")
        }
      }).then((0, _runloop.bind)(this, function (result) {
        that.transitionToRoute("predictionreport");
      }));
    }
  }
});

exports["default"] = _default;