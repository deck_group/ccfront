"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _service = require("@ember/service");

var _object = require("@ember/object");

var _jquery = _interopRequireDefault(require("jquery"));

var _runloop = require("@ember/runloop");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  bus: (0, _service.inject)("pubsub"),
  isReadonlyhour: false,
  isReadonlydaily: false,
  isReadonlyweekly: false,
  isReadonlymonthly: false,
  process: undefined,
  processname: undefined,
  urls: (0, _service.inject)("service"),
  isReadonlymonthlyCompute: (0, _object.computed)('isReadonlymonthly', function () {
    return this.get('isReadonlymonthly');
  }),
  isReadonlyweeklyCompute: (0, _object.computed)('isReadonlyweekly', function () {
    return this.get('isReadonlyweekly');
  }),
  isReadonlydailyCompute: (0, _object.computed)('isReadonlydaily', function () {
    return this.get('isReadonlydaily');
  }),
  isReadonlyhourCompute: (0, _object.computed)('isReadonlyhour', function () {
    return this.get('isReadonlyhour');
  }),
  init: function init() {
    this._super();

    this.get('bus').on('flagdata', this, this.recieve);
  },
  newmodelCompute: (0, _object.computed)('newmodel', function () {
    return this.get('newmodel');
  }),
  newhour: {},
  newmonth: {},
  newdaily: {},
  newweekly: {},
  recieve: function recieve(data, obj) {
    var newmodelv = {};

    if (data === "hour") {
      if (obj === true) {
        this.set("isReadonlyhour", false);
        this.set("isReadonlydaily", obj);
        this.set("isReadonlyweekly", obj);
        this.set("isReadonlymonthly", obj);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      } else {
        this.set("isReadonlyhour", obj);
        this.set("isReadonlydaily", obj);
        this.set("isReadonlyweekly", obj);
        this.set("isReadonlymonthly", obj);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      }

      this.set("newmodel", {});
      this.set("newmodel", newmodelv);
      this.get("newmodelCompute");
    } else if (data === "month") {
      if (obj === true) {
        this.set("isReadonlyhour", obj);
        this.set("isReadonlydaily", obj);
        this.set("isReadonlyweekly", obj);
        this.set("isReadonlymonthly", false);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      } else {
        this.set("isReadonlyhour", obj);
        this.set("isReadonlydaily", obj);
        this.set("isReadonlyweekly", obj);
        this.set("isReadonlymonthly", obj);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      }

      this.set("newmodel", {});
      this.set("newmodel", newmodelv);
      this.get("newmodelCompute");
    } else if (data === "week") {
      if (obj === true) {
        this.set("isReadonlyhour", obj);
        this.set("isReadonlydaily", obj);
        this.set("isReadonlyweekly", false);
        this.set("isReadonlymonthly", obj);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      } else {
        this.set("isReadonlyhour", obj);
        this.set("isReadonlydaily", obj);
        this.set("isReadonlyweekly", obj);
        this.set("isReadonlymonthly", obj);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      }

      this.set("newmodel", {});
      this.set("newmodel", newmodelv);
      this.get("newmodelCompute");
    } else if (data === "daily") {
      if (obj === true) {
        this.set("isReadonlyhour", obj);
        this.set("isReadonlydaily", false);
        this.set("isReadonlyweekly", obj);
        this.set("isReadonlymonthly", obj);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      } else {
        this.set("isReadonlyhour", obj);
        this.set("isReadonlydaily", obj);
        this.set("isReadonlyweekly", obj);
        this.set("isReadonlymonthly", obj);
        this.get("isReadonlyhourCompute");
        this.get("isReadonlydailyCompute");
        this.get("isReadonlyweeklyCompute");
        this.get("isReadonlymonthlyCompute");
      }
    }
  },
  actions: {
    discardroute: function discardroute() {
      this.transitionToRoute("scheduler");
    },
    save: function save(newhour, newmonth, newdaily, newweekly, status) {
      var _this = this;

      var newmodel = this.get('store').createRecord('processsheduler');
      newmodel.set("status", status);
      newmodel.set("jobname", this.get('processname'));
      console.log(newmodel);
      var flag = "";
      newmodel.set("process", this.get("process"));

      if (newhour.ishourly === true) {
        newmodel.set("isweekly", false);
        newmodel.set("ismonthly", false);
        newmodel.set("isdaily", false);
        newmodel.set("ishourly", newhour.ishourly);
        newmodel.set("month", "N/A");
        newmodel.set("monthday", "N/A");
        newmodel.set("weekday", "N/A");
        newmodel.set("hour", "N/A");
        newmodel.set("minute", newhour.minute);
        newmodel.set("second", newhour.second);
        newmodel.set("dayofweek", "N/A");
        flag = "hour";
      } else if (newmonth.ismonthly === true) {
        newmodel.set("isweekly", false);
        newmodel.set("ismonthly", newmonth.ismonthly);
        newmodel.set("isdaily", false);
        newmodel.set("ishourly", false);
        newmodel.set("month", "N/A");
        newmodel.set("monthday", "N/A");
        newmodel.set("weekday", "N/A");
        newmodel.set("hour", newmonth.hour);
        newmodel.set("minute", newmonth.minute);
        newmodel.set("second", newmonth.second);
        newmodel.set("monthday", newmonth.monthday);
        newmodel.set("dayofweek", "N/A");
        flag = "month";
      } else if (newdaily.isdaily === true) {
        newmodel.set("isweekly", false);
        newmodel.set("ismonthly", false);
        newmodel.set("isdaily", newdaily.isdaily);
        newmodel.set("ishourly", false);
        newmodel.set("month", "N/A");
        newmodel.set("monthday", "N/A");
        newmodel.set("weekday", "N/A");
        newmodel.set("hour", newdaily.hour);
        newmodel.set("minute", newdaily.minute);
        newmodel.set("second", newdaily.second);
        newmodel.set("monthday", "N/A");
        newmodel.set("dayofweek", "N/A");
        flag = "daily";
      } else {
        newmodel.set("isweekly", newweekly.isweekly);
        newmodel.set("ismonthly", false);
        newmodel.set("isdaily", false);
        newmodel.set("ishourly", false);
        newmodel.set("month", "N/A");
        newmodel.set("monthday", "N/A");
        newmodel.set("weekday", "N/A");
        newmodel.set("hour", newweekly.hour);
        newmodel.set("minute", newweekly.minute);
        newmodel.set("second", newweekly.second);
        newmodel.set("monthday", "N/A");
        newmodel.set("dayofweek", newweekly.dayofweek);
        flag = "week";
      }

      document.getElementById("overlay").style.display = "block";
      newmodel.save().then(function () {
        _this.transitionToRoute("scheduler"); // var hurl = ``
        // hurl = this.get("urls.urls") + 'billgenerations'
        // var process = this.get("process")
        // this.set("process", undefined)
        // $.ajax({
        //     method: "POST",
        //     url: hurl,
        //     data: {
        //         process: process,
        //         flag: flag
        //     }
        // }).then(bind(this, (data) => {
        //     this.set("newhour", {})
        //     this.set("newmonth", {})
        //     this.set("newdaily", {})
        //     this.set("newweekly", {})
        //     document.getElementById("overlay").style.display = "none";
        //     this.transitionToRoute("scheduler")
        // }))

      });
    }
  }
});

exports["default"] = _default;