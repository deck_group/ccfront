"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _service = require("@ember/service");

var _object = require("@ember/object");

var _environment = _interopRequireDefault(require("../config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  websocketser: (0, _service.inject)("websocket"),
  routerService: (0, _service.inject)('router'),
  statusDict: undefined,
  websocket: new WebSocket(_environment["default"].WEB_HOST),
  initProg: true,
  init: function init() {
    this._super.apply(this, arguments);

    this.get("websocketser").on('updatedstatus', this, this.recievedupdatedstatus);
  },
  recievedupdatedstatus: function recievedupdatedstatus(dataarray) {
    this.set('statusDict', dataarray);
  },
  waitForSocketConnection: function waitForSocketConnection(socket, callback) {
    var self = this;

    if (socket.readyState === 1) {
      if (callback != null) {
        callback();
      }
    } else {
      Ember.run.later(function () {
        self.waitForSocketConnection(socket, callback);
      }, 1000);
    }
  },
  statusDictComputed: (0, _object.computed)('statusDict', function () {
    if (this.get("initProg") === true) {
      var that = this;
      var ws = this.get('websocket');
      this.waitForSocketConnection(ws, function () {
        ws.send("allstatuses");
      });

      ws.onmessage = function (evt) {
        var data = JSON.parse(evt.data);

        if (data['channel'] === "allstatuses") {
          that.set('statusDict', data['data']);
          that.set('initProg', false);
          return that.get('statusDict');
        }
      };
    } else {
      return this.get('statusDict');
    }
  }),
  actions: {
    createroute: function createroute() {
      this.set('initProg', true);
      this.transitionToRoute('scheduler-new');
    },
    view: function view(id) {
      this.set('initProg', true);
      this.transitionToRoute('scheduler-view', id); // this.get('routerService').urlFor('scheduler-view', id)
    },
    logdetail: function logdetail() {
      this.set('initProg', true);
      this.transitionToRoute('logdetail');
    }
  }
});

exports["default"] = _default;