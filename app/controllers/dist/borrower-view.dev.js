"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  actions: {
    discardroute: function discardroute() {
      this.transitionToRoute("borrower");
    },
    pretab: function pretab() {
      var detail = document.getElementById('notebook_page_89');
      detail.setAttribute("class", "tab-pane");
      var contract = document.getElementById('notebook_page_90');
      contract.setAttribute("class", "tab-pane");
      var crm = document.getElementById('notebook_page_91');
      crm.setAttribute("class", "tab-pane");
      var detailicon = document.getElementById('detail');
      detailicon.setAttribute("class", "competition-overview__nav-item");
      var contracticon = document.getElementById('contract');
      contracticon.setAttribute("class", "competition-overview__nav-item");
      var crmicon = document.getElementById('crm');
      crmicon.setAttribute("class", "competition-overview__nav-item");
      var scheduler = document.getElementById('notebook_page_92');
      scheduler.setAttribute("class", "tab-pane");
      var scheicon = document.getElementById('sche');
      scheicon.setAttribute("class", "competition-overview__nav-item");
      var billgen = document.getElementById('notebook_page_93');
      billgen.setAttribute("class", "tab-pane");
      var billicon = document.getElementById('bill');
      billicon.setAttribute("class", "competition-overview__nav-item");
      var paygen = document.getElementById('notebook_page_94');
      paygen.setAttribute("class", "tab-pane");
      var payicon = document.getElementById('payment');
      payicon.setAttribute("class", "competition-overview__nav-item");
      var predgen = document.getElementById('notebook_page_95');
      predgen.setAttribute("class", "active");
      var preicon = document.getElementById('pred');
      preicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected");
    },
    paytab: function paytab() {
      var detail = document.getElementById('notebook_page_89');
      detail.setAttribute("class", "tab-pane");
      var contract = document.getElementById('notebook_page_90');
      contract.setAttribute("class", "tab-pane");
      var crm = document.getElementById('notebook_page_91');
      crm.setAttribute("class", "tab-pane");
      var detailicon = document.getElementById('detail');
      detailicon.setAttribute("class", "competition-overview__nav-item");
      var contracticon = document.getElementById('contract');
      contracticon.setAttribute("class", "competition-overview__nav-item");
      var crmicon = document.getElementById('crm');
      crmicon.setAttribute("class", "competition-overview__nav-item");
      var scheduler = document.getElementById('notebook_page_92');
      scheduler.setAttribute("class", "tab-pane");
      var scheicon = document.getElementById('sche');
      scheicon.setAttribute("class", "competition-overview__nav-item");
      var billgen = document.getElementById('notebook_page_93');
      billgen.setAttribute("class", "tab-pane");
      var billicon = document.getElementById('bill');
      billicon.setAttribute("class", "competition-overview__nav-item");
      var paygen = document.getElementById('notebook_page_94');
      paygen.setAttribute("class", "active");
      var payicon = document.getElementById('payment');
      payicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected");
      var predgen = document.getElementById('notebook_page_95');
      predgen.setAttribute("class", "tab-pane");
      var preicon = document.getElementById('pred');
      preicon.setAttribute("class", "competition-overview__nav-item");
    },
    billtab: function billtab() {
      var detail = document.getElementById('notebook_page_89');
      detail.setAttribute("class", "tab-pane");
      var contract = document.getElementById('notebook_page_90');
      contract.setAttribute("class", "tab-pane");
      var crm = document.getElementById('notebook_page_91');
      crm.setAttribute("class", "tab-pane");
      var detailicon = document.getElementById('detail');
      detailicon.setAttribute("class", "competition-overview__nav-item");
      var contracticon = document.getElementById('contract');
      contracticon.setAttribute("class", "competition-overview__nav-item");
      var crmicon = document.getElementById('crm');
      crmicon.setAttribute("class", "competition-overview__nav-item");
      var scheduler = document.getElementById('notebook_page_92');
      scheduler.setAttribute("class", "tab-pane");
      var scheicon = document.getElementById('sche');
      scheicon.setAttribute("class", "competition-overview__nav-item");
      var billgen = document.getElementById('notebook_page_93');
      billgen.setAttribute("class", "active");
      var billicon = document.getElementById('bill');
      billicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected");
      var paygen = document.getElementById('notebook_page_94');
      paygen.setAttribute("class", "tab-pane");
      var payicon = document.getElementById('payment');
      payicon.setAttribute("class", "competition-overview__nav-item");
      var predgen = document.getElementById('notebook_page_95');
      predgen.setAttribute("class", "tab-pane");
      var preicon = document.getElementById('pred');
      preicon.setAttribute("class", "competition-overview__nav-item");
    },
    schetab: function schetab() {
      var detail = document.getElementById('notebook_page_89');
      detail.setAttribute("class", "tab-pane");
      var contract = document.getElementById('notebook_page_90');
      contract.setAttribute("class", "tab-pane");
      var crm = document.getElementById('notebook_page_91');
      crm.setAttribute("class", "tab-pane");
      var detailicon = document.getElementById('detail');
      detailicon.setAttribute("class", "competition-overview__nav-item");
      var contracticon = document.getElementById('contract');
      contracticon.setAttribute("class", "competition-overview__nav-item");
      var crmicon = document.getElementById('crm');
      crmicon.setAttribute("class", "competition-overview__nav-item");
      var scheduler = document.getElementById('notebook_page_92');
      scheduler.setAttribute("class", "active");
      var scheicon = document.getElementById('sche');
      scheicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected");
      var billgen = document.getElementById('notebook_page_93');
      billgen.setAttribute("class", "tab-pane");
      var billicon = document.getElementById('bill');
      billicon.setAttribute("class", "competition-overview__nav-item");
      var paygen = document.getElementById('notebook_page_94');
      paygen.setAttribute("class", "tab-pane");
      var payicon = document.getElementById('payment');
      payicon.setAttribute("class", "competition-overview__nav-item");
      var predgen = document.getElementById('notebook_page_95');
      predgen.setAttribute("class", "tab-pane");
      var preicon = document.getElementById('pred');
      preicon.setAttribute("class", "competition-overview__nav-item");
    },
    detailtab: function detailtab() {
      var detail = document.getElementById('notebook_page_89');
      detail.setAttribute("class", "active");
      var contract = document.getElementById('notebook_page_90');
      contract.setAttribute("class", "tab-pane");
      var crm = document.getElementById('notebook_page_91');
      crm.setAttribute("class", "tab-pane");
      var detailicon = document.getElementById('detail');
      detailicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected");
      var contracticon = document.getElementById('contract');
      contracticon.setAttribute("class", "competition-overview__nav-item");
      var crmicon = document.getElementById('crm');
      crmicon.setAttribute("class", "competition-overview__nav-item");
      var scheduler = document.getElementById('notebook_page_92');
      scheduler.setAttribute("class", "tab-pane");
      var scheicon = document.getElementById('sche');
      scheicon.setAttribute("class", "competition-overview__nav-item");
      var billgen = document.getElementById('notebook_page_93');
      billgen.setAttribute("class", "tab-pane");
      var billicon = document.getElementById('bill');
      billicon.setAttribute("class", "competition-overview__nav-item");
      var paygen = document.getElementById('notebook_page_94');
      paygen.setAttribute("class", "tab-pane");
      var payicon = document.getElementById('payment');
      payicon.setAttribute("class", "competition-overview__nav-item");
      var predgen = document.getElementById('notebook_page_95');
      predgen.setAttribute("class", "tab-pane");
      var preicon = document.getElementById('pred');
      preicon.setAttribute("class", "competition-overview__nav-item");
    },
    contracttab: function contracttab() {
      var detail = document.getElementById('notebook_page_89');
      detail.setAttribute("class", "tab-pane");
      var contract = document.getElementById('notebook_page_90');
      contract.setAttribute("class", "active");
      var crm = document.getElementById('notebook_page_91');
      crm.setAttribute("class", "tab-pane");
      var detailicon = document.getElementById('detail');
      detailicon.setAttribute("class", "competition-overview__nav-item");
      var contracticon = document.getElementById('contract');
      contracticon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected");
      var crmicon = document.getElementById('crm');
      crmicon.setAttribute("class", "competition-overview__nav-item");
      var scheduler = document.getElementById('notebook_page_92');
      scheduler.setAttribute("class", "tab-pane");
      var scheicon = document.getElementById('sche');
      scheicon.setAttribute("class", "competition-overview__nav-item");
      var billgen = document.getElementById('notebook_page_93');
      billgen.setAttribute("class", "tab-pane");
      var billicon = document.getElementById('bill');
      billicon.setAttribute("class", "competition-overview__nav-item");
      var paygen = document.getElementById('notebook_page_94');
      paygen.setAttribute("class", "tab-pane");
      var payicon = document.getElementById('payment');
      payicon.setAttribute("class", "competition-overview__nav-item");
      var predgen = document.getElementById('notebook_page_95');
      predgen.setAttribute("class", "tab-pane");
      var preicon = document.getElementById('pred');
      preicon.setAttribute("class", "competition-overview__nav-item");
    },
    crmtab: function crmtab() {
      var detail = document.getElementById('notebook_page_89');
      detail.setAttribute("class", "tab-pane");
      var contract = document.getElementById('notebook_page_90');
      contract.setAttribute("class", "tab-pane");
      var crm = document.getElementById('notebook_page_91');
      crm.setAttribute("class", "active");
      var scheduler = document.getElementById('notebook_page_92');
      scheduler.setAttribute("class", "tab-pane");
      var billgen = document.getElementById('notebook_page_93');
      billgen.setAttribute("class", "tab-pane");
      var billicon = document.getElementById('bill');
      billicon.setAttribute("class", "competition-overview__nav-item");
      var detailicon = document.getElementById('detail');
      detailicon.setAttribute("class", "competition-overview__nav-item");
      var contracticon = document.getElementById('contract');
      contracticon.setAttribute("class", "competition-overview__nav-item");
      var crmicon = document.getElementById('crm');
      crmicon.setAttribute("class", "competition-overview__nav-item competition-overview__nav-item--selected");
      var scheicon = document.getElementById('sche');
      scheicon.setAttribute("class", "competition-overview__nav-item");
      var paygen = document.getElementById('notebook_page_94');
      paygen.setAttribute("class", "tab-pane");
      var payicon = document.getElementById('payment');
      payicon.setAttribute("class", "competition-overview__nav-item");
      var predgen = document.getElementById('notebook_page_95');
      predgen.setAttribute("class", "tab-pane");
      var preicon = document.getElementById('pred');
      preicon.setAttribute("class", "competition-overview__nav-item");
    }
  }
});

exports["default"] = _default;