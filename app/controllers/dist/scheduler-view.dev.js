"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _service = require("@ember/service");

var _object = require("@ember/object");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  urls: (0, _service.inject)("service"),
  componentNames: undefined,
  stopButton: "false",
  websocket: (0, _service.inject)("websocket"),
  disabled: "false",
  startWatch: false,
  estTime: '00:00:00',
  time: '00:00:00',
  estTimetotal: 0,
  totalSeconds: 0,
  initProg: true,
  initProgBar: true,
  intervalID: undefined,
  intervalIDTWO: undefined,
  init: function init() {
    this._super.apply(this, arguments);

    this.get("websocket").on('setstopjobpub', this, this.recievedstopbtn);
    this.get("websocket").on('jobschedule', this, this.recievedLogData);
    this.get("websocket").on('jobschstop', this, this.setProgressBar);
    this.get("websocket").on('esttimepub', this, this.setestTime);
    this.get("websocket").on('clearComArrayReply', this, this.clearcomarrayrep);
    this.get("websocket").on('stopbtntrue', this, this.stopbtntrue);
  },
  stopbtntrue: function stopbtntrue(dataarray) {
    if (dataarray[0] === this.get('process').id) {
      clearInterval(this.get('intervalID'));
      clearInterval(this.get('intervalIDTWO'));
      this.set('stopButton', true);
      this.get('stopButtonComputed');
    }
  },
  componentNamesComputed: (0, _object.computed)('componentNames', function () {
    return this.get('componentNames');
  }),
  stopButtonComputed: (0, _object.computed)('stopButton', function () {
    return this.get('stopButton');
  }),
  stopWatchComputed: (0, _object.computed)('time', function () {
    return this.get('time');
  }),
  estTimeCompute: (0, _object.computed)('estTime', function () {
    return this.get('estTime');
  }),
  clearcomarrayrep: function clearcomarrayrep(dataarray) {
    if (this.get('schid') === dataarray[0]) {
      this.set('componentNames', undefined);
      this.get('componentNamesComputed');
    }
  },
  setestTime: function setestTime(dataarray) {
    if (this.get('schid') === dataarray[0]) {
      this.set('estTimetotal', dataarray[2]);
      this.set('estTime', dataarray[1]);
      this.get('estTimeCompute');
    }
  },
  myCounter: function myCounter() {
    var totalSeconds = this.get('totalSeconds');
    totalSeconds = totalSeconds + 1;
    this.set('totalSeconds', totalSeconds);
    var hr = "";
    var min = "";
    var sec = "";
    var hour = Math.floor(totalSeconds / 3600);
    var minute = Math.floor((totalSeconds - hour * 3600) / 60);
    var seconds = totalSeconds - (hour * 3600 + minute * 60);

    if (hour < 10) {
      hr = "0" + hour;
    } else {
      hr = hour;
    }

    if (minute < 10) {
      min = "0" + minute;
    } else {
      min = minute;
    }

    if (seconds < 10) {
      sec = "0" + seconds;
    } else {
      sec = seconds;
    }

    var time = hr + ":" + min + ":" + sec;
    this.set('time', time);
  },
  progressLoad: function progressLoad() {
    var totalSeconds = this.get('totalSeconds');
    var elem = document.getElementById("myBar");
    var per = totalSeconds / this.get('estTimetotal') * 100;

    if (per >= 100) {
      clearInterval(this.get('intervalIDTWO'));
    } else {
      elem.style.width = Math.round(per, 2) + "%";
      elem.innerHTML = Math.round(per, 2) + "%";
    }
  },
  recievedstopbtn: function recievedstopbtn(dataarray) {
    if (dataarray[0] === this.get('process').id) {
      this.set('stopButton', dataarray[1]);
      this.get('stopButtonComputed');
    }
  },
  setProgressBar: function setProgressBar(dataarray) {
    if (dataarray[0] === this.get('schid')) {
      clearInterval(this.get('intervalID'));
      clearInterval(this.get('intervalIDTWO'));
      this.set('progress', dataarray[1]);
      this.get('progressComputed');
      var newdict = {};
      newdict[this.get('process').id] = "0";
      this.get('websocket').update(JSON.stringify(newdict));
      var elem = document.getElementById("myBar");
      elem.style.width = 100 + "%";
      elem.innerHTML = 100 + "%";
    }
  },
  recievedLogData: function recievedLogData(dataarray) {
    if (dataarray[3] == this.get('schid')) {
      this.set('progress', "true");
      this.get('progressComputed');
      var array = [];

      if (this.get('componentNames') !== undefined) {
        for (var i = 0; i < this.get('componentNames').length; i++) {
          array.push(this.get('componentNames')[i]);
        }
      }

      var dict = {};
      dict['dt'] = dataarray[0];
      dict['time'] = dataarray[1];
      dict['description'] = dataarray[2];
      array.push(dict);
      this.set('componentNames', array);
      this.get('componentNamesComputed');
    }
  },
  progress: "false",
  progressComputed: (0, _object.computed)('progress', function () {
    if (this.get('initProg') === true) {
      if (this.get('jobstatus').status === "0") {
        console.log("job status true");
        this.set('progress', "false");
        this.set("initProg", false);
      } else {
        this.set('progress', "true");
        this.set("initProg", false);
      }

      return this.get('progress');
    } else {
      return this.get('progress');
    }
  }),
  actions: {
    discardroute: function discardroute() {
      this.set('totalSeconds', 0);
      this.set('componentNames', undefined);
      this.set('estTime', '00:00:00');
      this.set('time', '00:00:00');
      this.set("progress", "false");
      this.get("progressComputed");
      this.set("initProg", true);
      this.set('initProgBar', true);
      clearInterval(this.get('intervalID'));
      clearInterval(this.get('intervalIDTWO'));
      this.transitionToRoute("scheduler");
    },
    stop: function stop(jobstatus, id) {
      clearInterval(this.get('intervalID'));
      clearInterval(this.get('intervalIDTWO'));
      this.set('progress', 'false');
      this.get('progressComputed');
      this.get('websocket').stopprocess(id);
      var newdict = {};
      newdict[id] = "0";
      this.get('websocket').update(JSON.stringify(newdict));
      jobstatus.set('status', '0');
      jobstatus.save();
    },
    run: function run(id) {
      this.set('progress', "true");
      this.get('progressComputed');
      this.set('totalSeconds', 0);
      this.set('time', '00:00:00');
      var newdict = {};
      newdict[id] = "1";
      this.get('websocket').update(JSON.stringify(newdict));
      var elem = document.getElementById("myBar");
      elem.style.width = 0 + "%";
      elem.innerHTML = 0 + "%";
      var interID = setInterval(this.myCounter.bind(this), 1000);
      var interID2 = setInterval(this.progressLoad.bind(this), 10);
      this.set('intervalID', interID);
      this.set('intervalIDTWO', interID2);
      this.set('estTime', undefined);
      this.get('isOnline');
      this.set('initProgBar', false);
      this.get('websocket').clearComponentArray(id);
      this.get('store').query('processlog', {
        'process': id
      });
    }
  }
});

exports["default"] = _default;