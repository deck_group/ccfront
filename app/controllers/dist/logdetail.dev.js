"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _controller = _interopRequireDefault(require("@ember/controller"));

var _object = require("@ember/object");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _controller["default"].extend({
  queryParams: ['page', 'size'],
  page: 1,
  size: 10,
  count: (0, _object.computed)('joblogs.meta.pagination.last.number', 'joblogs.meta.pagination.self.number', function () {
    var total = this.get('joblogs.meta.pagination.last.number') || this.get('joblogs.meta.pagination.self.number');
    if (!total) return [];
    return new Array(total + 1).join('x').split('').map(function (e, i) {
      return i + 1;
    });
  }),
  actions: {
    discardroute: function discardroute() {
      this.transitionToRoute("scheduler");
    },
    view: function view(id) {
      this.transitionToRoute("logdetail-view", id);
    }
  }
});

exports["default"] = _default;