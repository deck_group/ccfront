import Controller from '@ember/controller';
import $ from 'jquery';
import config from '../config/environment';
import {
    bind
} from '@ember/runloop';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';

export default Controller.extend({
    websocket: service("websocket"),
    init() {
        this._super(...arguments)
        this.set('message',undefined)
        this.get('mesasgeCompute')
        this.get("websocket").on('predictionlog', this, this.recieved)
        this.get("websocket").on('endbatchlog', this, this.recievedbatch)
    },
    modelname: ``,
    start: ``,
    message:undefined,
    componentNames: undefined,
    end: ``,
    componentNamesComputed: computed('componentNames', function () {
        return this.get('componentNames')
    }),

    mesasgeCompute: computed('message', function () {
        return this.get('message')
    }),

    recievedbatch(data){
        this.set('message',data)
        this.get('mesasgeCompute')
    },
    recieved(dataarray) {
        var array = []
        if (this.get('componentNames') !== undefined) {
            for (var i = 0; i < this.get('componentNames').length; i++) {
                array.push(this.get('componentNames')[i])
            }
        }
        var dict = {}
        dict['description'] = dataarray
        array.push(dict)
        this.set('componentNames', array)
        this.get('componentNamesComputed')
    },
    actions: {

        discardroute() {
            this.set('message',undefined)
            this.get('mesasgeCompute')
            this.set('start', ``)
            this.set('end', ``)
            this.set('componentNames', undefined)
            this.get('componentNamesComputed')
            this.set('modelname',``)
            this.transitionToRoute("home")
         },
        run(modelname, start, end) {
            this.set('message',undefined)
            this.get('mesasgeCompute')
            // document.getElementById("overlay").style.display = "block";
            var that = this
            var url = config.API_OTHER_HOST + 'batchpredictions'
            var that = this
            $.ajax({
                method: "POST",
                url: url,
                data: {
                    start: start,
                    end: end,
                    model: modelname,
                    user: localStorage.getItem("user")
                }
            }).then(bind(this, (result) => {
                localStorage.setItem('item', undefined)
                localStorage.setItem('item', 'predicReport')
                that.set('start', ``)
                that.set('end', ``)
                that.set('componentNames', undefined)
                that.get('componentNamesComputed')
                that.set('modelname',``)

            }))
        }
    }
});
