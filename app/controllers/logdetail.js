import Controller from '@ember/controller';
import { computed } from '@ember/object';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    count: computed('joblogs.meta.pagination.last.number', 'joblogs.meta.pagination.self.number', function () {
        const total = this.get('joblogs.meta.pagination.last.number') || this.get('joblogs.meta.pagination.self.number')
        if (!total) return [];
        return new Array(total + 1).join('x').split('').map((e, i) => i + 1);
    }),

    actions: {
        discardroute() {
            this.transitionToRoute("scheduler")
        },
        view(id) {
            this.transitionToRoute("logdetail-view", id)
        }
    }
});
