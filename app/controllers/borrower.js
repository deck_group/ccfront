import Controller from '@ember/controller';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    data:undefined,
    actions:{
        view(borrowerid){
            this.transitionToRoute("borrower-view",borrowerid)
        },
        search(data){
            var search=data
            this.set("data",undefined)
            this.transitionToRoute('borrower-params',search)
            
        }
    }
});
