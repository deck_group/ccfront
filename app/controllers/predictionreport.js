import Controller from '@ember/controller';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    loantype: `N/A`,
    loanname: `N/A`,
    province: `N/A`,
    district: `N/A`,
    actions: {
        filter(loantype, province, district) {
            this.set('loantype', 'N/A')
            this.set('province', 'N/A')
            this.set('district', 'N/A')
            this.transitionToRoute('predictionreport-param', loantype, province, district)
        },
        view(borrowerid) {
            this.transitionToRoute("borrower-view", borrowerid)
        }
    }
});
