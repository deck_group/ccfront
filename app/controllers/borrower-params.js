import Controller from '@ember/controller';
import { inject as service } from '@ember/service';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    actions: {
        view(borrowerid) {
            this.transitionToRoute("borrower-view", borrowerid)
        },
        clear() {
            this.transitionToRoute("borrower")
        }
    }
});
