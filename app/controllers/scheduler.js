import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import config from '../config/environment';

export default Controller.extend({
    websocketser: service("websocket"),
    routerService: service('router'),
    statusDict: undefined,
    websocket: new WebSocket(config.WEB_HOST),
    initProg: true,
    init() {
        this._super(...arguments)
        this.get("websocketser").on('updatedstatus', this, this.recievedupdatedstatus)
    },
    recievedupdatedstatus(dataarray) {
        this.set('statusDict', dataarray)
    },

    waitForSocketConnection: function (socket, callback) {
        var self = this;
        if (socket.readyState === 1) {
            if (callback != null) {
                callback();
            }
        } else {
            Ember.run.later((function () {
                self.waitForSocketConnection(socket, callback)
            }), 1000);
        }

    },
    statusDictComputed: computed('statusDict', function () {
        if (this.get("initProg") === true) {
            var that = this
            var ws = this.get('websocket')
            this.waitForSocketConnection(ws, function () {
                ws.send("allstatuses")

            });

            ws.onmessage = function (evt) {

                var data = JSON.parse(evt.data)
                if (data['channel'] === "allstatuses") {
                    that.set('statusDict', data['data'])
                    that.set('initProg', false)
                    return that.get('statusDict')
                }

            }
        }
        else {
            return this.get('statusDict')
        }

    }),
    actions: {
        createroute() {
            this.set('initProg', true)
            this.transitionToRoute('scheduler-new')
        },
        view(id) {
            this.set('initProg', true)
            this.transitionToRoute('scheduler-view',id)
            // this.get('routerService').urlFor('scheduler-view', id)
        },
        logdetail() {
            this.set('initProg', true)
            this.transitionToRoute('logdetail')
        }
    }
});
