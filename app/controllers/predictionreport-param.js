import Controller from '@ember/controller';

export default Controller.extend({
    queryParams: ['page', 'size'],
    page: 1,
    size: 10,
    actions:{
        clear(){
            this.transitionToRoute('predictionreport')
        },
        view(borrowerid){
            this.transitionToRoute("borrower-view",borrowerid)
        }
    }
});
