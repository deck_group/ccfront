import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validate: function (value, isPresence, label, regexformat, checkleadingSpace, minLength, maxLength) {
        var msgArray = []
        if (isPresence === true) {
            msgArray.push(this.presence(value, label))
        }
        if (checkleadingSpace === true) {
            msgArray.push(this.ifLeadingSpace(value, label))
        }

        msgArray.push(this.formatRule(value, regexformat, label))
        msgArray.push(this.checkingLength(value, minLength, maxLength, label))
        var filtered = msgArray.filter(function (el) {
            return el != "";
        });
        return filtered
    },

    presence(value, label) {
        var msg = ''
        if (value === undefined || value === "") {
            msg = label + ` is required`
        }
        return msg

    },

    ifLeadingSpace(value, label) {
        var msg = ''
        if (value !== undefined) {
            var newtext = value.trim()
            if (newtext.length == 0) {
                msg = label + ` should not have only space`
            }
        }
        return msg
    },
    formatRule(value, regexformat, label) {
        var msg = ''
        var patt = new RegExp(regexformat)
        console.log(patt)
        console.log(patt.test(value))
        if (patt.test(value) === false) {
            msg = label + ` format is not correct`
        }
        return msg

    },

    checkingLength(value, minthreshold, maxthreshold, label) {
        var msg = ''
        if (value !== 'N/A') {
            if (value.length !== maxthreshold) {
                if (value.length !== minthreshold) {
                    msg = label + ` must have length ` + maxthreshold.toString() + ` or ` + minthreshold.toString()
                }

            }
        }
        return msg
    },

});
