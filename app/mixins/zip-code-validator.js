import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validate: function (value, isPresence, label, regexformat, checkleadingSpace, length) {
        var msgArray = []
        if (isPresence === true) {
            msgArray.push(this.presence(value, label))
        }
        if (checkleadingSpace === true) {
            msgArray.push(this.ifLeadingSpace(value, label))
        }
        msgArray.push(this.formatRule(value, regexformat, label))
        msgArray.push(this.checkingLength(value, length, label))

        var filtered = msgArray.filter(function (el) {
            return el != "";
        });

        return filtered
    },

    checkingLength(value, length, label) {
        var msg = ''
        if (value !== undefined) {
            if (value.length !== length) {
                msg = label + ` should have ` + length.toString() + ` digits `
            }

        }

        return msg
    },

    formatRule(value, regexformat, label) {
        var msg = ''
        var patt = new RegExp(regexformat)
        if (patt.test(value) === false) {
            msg = label + ` format is not correct`
        }
        return msg

    },

    ifLeadingSpace(value, label) {
        var msg = ''
        if (value !== undefined) {
            var newtext = value.trim()
            if (newtext.length == 0) {
                msg = label + ` should not have only space`
            }
        }
        return msg
    },

    presence(value, label) {
        var msg = ''
        if (value === undefined || value === "") {
            msg = label + ` is required`
        }
        return msg

    },
});
