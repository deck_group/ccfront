import Mixin from '@ember/object/mixin';

export default Mixin.create({
    validate: function (value, isPresence, label, regexformat,min) {
        var msgArray = []
        if (isPresence === true) {
            msgArray.push(this.presence(value, label))
        }
        msgArray.push(this.formatRule(value, regexformat, label))
        msgArray.push(this.checkmin(value, min, label))
        var filtered = msgArray.filter(function (el) {
            return el != "";
        });
        


        return filtered
    },

    formatRule(value, regexformat, label) {
        var msg = ''
        var patt = new RegExp(regexformat)
        if (patt.test(value) === false) {
            msg = label + ` format is not correct`
        }
        return msg

    },

    presence(value, label) {
        var msg = ''
        if (value === undefined || value === "") {
            msg = label + ` is required`
        }
        return msg

    },

    checkmin(value,min,label){
        var msg = ''
        if (parseInt(value) < min) {
            msg = label + ` should have atleast  ` + min.toString()
        }
        return msg
    }
});
