"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _mixin = _interopRequireDefault(require("@ember/object/mixin"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _mixin["default"].create({
  validate: function validate(value, isPresence, label, regexformat, min) {
    var msgArray = [];

    if (isPresence === true) {
      msgArray.push(this.presence(value, label));
    }

    msgArray.push(this.formatRule(value, regexformat, label));
    msgArray.push(this.checkmin(value, min, label));
    var filtered = msgArray.filter(function (el) {
      return el != "";
    });
    return filtered;
  },
  formatRule: function formatRule(value, regexformat, label) {
    var msg = '';
    var patt = new RegExp(regexformat);

    if (patt.test(value) === false) {
      msg = label + " format is not correct";
    }

    return msg;
  },
  presence: function presence(value, label) {
    var msg = '';

    if (value === undefined || value === "") {
      msg = label + " is required";
    }

    return msg;
  },
  checkmin: function checkmin(value, min, label) {
    var msg = '';

    if (parseInt(value) < min) {
      msg = label + " should have atleast  " + min.toString();
    }

    return msg;
  }
});

exports["default"] = _default;