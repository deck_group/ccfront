import Service from '@ember/service';
import Evented from '@ember/object/evented';

export default Service.extend(Evented, {
    interestTypepublish: function (content) {
        this.trigger('interestType', content);
    },
    principalAmtPublish: function (content) {
        this.trigger('principalAmt', content);
    },

    ratePublish: function (content, object) {
        this.trigger('rate', content, object);
    },

    numberPublish: function (content, object) {
        this.trigger('searchNumber', content, object);
    },

    valuePublish: function (content, object) {
        this.trigger('valueset', content, object);
    },

    sentdataPublish: function (content, object) {
        this.trigger('sentdata', content, object);
    },
    flagPublish: function (content, object) {
        this.trigger('flagdata', content, object);
    },

    jobPublish: function (content) {
        console.log(content)
    },
    conditionalPublish(content) {
        this.trigger('conditional', content);
    }





});
