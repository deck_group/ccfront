import Service from '@ember/service';
import Evented from '@ember/object/evented';
import config from '../config/environment';

export default Service.extend(Evented, {
    websocket: new WebSocket(config.WEB_HOST),
    init() {
        this._super(...arguments)
        var that = this
        var ws = this.get('websocket')
        ws.onmessage = function (evt) {
            var data = JSON.parse(evt.data)
            
            if (data['channel'] === "schedulerlog") {
                that.jobsch(data['data'])
            } else if (data['channel'] === "schedulerover") {
                that.jobschstop(data['data'])
            } else if (data['channel'] === 'setstopjob') {
                that.setstopjob(data['data'])
            }
            else if (data['channel'] === 'esttime') {
                that.sesttimejob(data['data'])
            }
            else if (data['channel'] === 'clearcomdatarep') {
                that.clearComArrayReply(data['data'])
            }
            else if (data['channel'] === 'stopbtntrue') {
                that.stopbtntrue(data['data'])
            }
            else if (data['channel'] === 'statusdbupdated') {
                that.updatedstatus(data['data'])
            }
            else if (data['channel'] === 'predictionlog') {
                that.updatedpredictionlog(data['data'])
            }
            else if (data['channel'] === 'endbatch') {
                that.upendbatchlog(data['data'])
            }

            
        }
    },
    upendbatchlog(content){
        this.trigger('endbatchlog', content)
    },
    updatedpredictionlog(content){
        this.trigger('predictionlog', content)
    },
    updatedstatus(content) {
        this.trigger('updatedstatus', content)
    },
    update(content) {
        var newdata = "jobstatus" + "-" + content
        var ws = this.get('websocket')
        this.waitForSocketConnection(ws, function () {
            ws.send(newdata)
        });

    },


    stopbtntrue(content) {
        this.trigger('stopbtntrue',content.split("-"))
    },
    clearComArrayReply(content) {
        this.trigger('clearComArrayReply',content.split("-"))
    },
    clearComponentArray(id) {
        var ws = this.get('websocket')
        var txt=id+'-clearcomdata'
        this.waitForSocketConnection(ws, function () {
            ws.send(txt)
        });

    },
    sesttimejob(content) {
        this.trigger('esttimepub', content.split("-"))
    },
    setstopjob(content) {
        this.trigger('setstopjobpub', content.split("-"))
    },
    stopprocess(id) {
        var ws = this.get('websocket')
        var txt=id+"-canceljob"
        this.waitForSocketConnection(ws, function () {
            ws.send(txt)
        });

    },
    jobsch(content) {
        this.trigger('jobschedule', content.split("-"))
    },

    jobschstop(content) {
        this.trigger('jobschstop', content.split("-"))
    },

    waitForSocketConnection: function (socket, callback) {
        var self = this;
        if (socket.readyState === 1) {
            if (callback != null) {
                callback();
            }
        } else {
            Ember.run.later((function () {
                self.waitForSocketConnection(socket, callback)
            }), 5000);
        }

    }

});
