import Service from '@ember/service';
import config from '../config/environment';

export default Service.extend({
    websocket: new WebSocket(config.WEB_HOST),
    loginpub: function (content) {
        var ws = this.get('websocket')
        var newdata = "login" + "-" + JSON.stringify(content)
        this.waitForSocketConnection(ws, function () {
            console.log("message sent!!!");
            ws.send(newdata)
        });
    },
    waitForSocketConnection: function (socket, callback) {
        var self = this;
        if (socket.readyState === 1) {
            console.log("Connection is made")
            if (callback != null) {
                callback();
            }
        } else {
            Ember.run.later((function () {
                self.waitForSocketConnection(socket, callback)
            }), 5);
        }

    }
});
