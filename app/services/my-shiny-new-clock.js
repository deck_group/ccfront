import Clock from 'ember-cli-clock/services/clock';

export default Clock.extend({
  interval: 1000,
  stop(intervalId){
    clearInterval(intervalId)
  }
});
