import Service from '@ember/service';
import config from '../config/environment';

export default Service.extend({
    signout: config.API_HOST,
    url: config.API_OTHER_HOST,
    websocket:config.WEB_HOST
});
