"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

var _evented = _interopRequireDefault(require("@ember/object/evented"));

var _environment = _interopRequireDefault(require("../config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend(_evented["default"], {
  websocket: new WebSocket(_environment["default"].WEB_HOST),
  init: function init() {
    this._super.apply(this, arguments);

    var that = this;
    var ws = this.get('websocket');

    ws.onmessage = function (evt) {
      var data = JSON.parse(evt.data);

      if (data['channel'] === "schedulerlog") {
        that.jobsch(data['data']);
      } else if (data['channel'] === "schedulerover") {
        that.jobschstop(data['data']);
      } else if (data['channel'] === 'setstopjob') {
        that.setstopjob(data['data']);
      } else if (data['channel'] === 'esttime') {
        that.sesttimejob(data['data']);
      } else if (data['channel'] === 'clearcomdatarep') {
        that.clearComArrayReply(data['data']);
      } else if (data['channel'] === 'stopbtntrue') {
        that.stopbtntrue(data['data']);
      } else if (data['channel'] === 'statusdbupdated') {
        that.updatedstatus(data['data']);
      }
    };
  },
  updatedstatus: function updatedstatus(content) {
    this.trigger('updatedstatus', content);
  },
  update: function update(content) {
    var newdata = "jobstatus" + "-" + content;
    var ws = this.get('websocket');
    this.waitForSocketConnection(ws, function () {
      ws.send(newdata);
    });
  },
  stopbtntrue: function stopbtntrue(content) {
    this.trigger('stopbtntrue', content.split("-"));
  },
  clearComArrayReply: function clearComArrayReply(content) {
    this.trigger('clearComArrayReply', content.split("-"));
  },
  clearComponentArray: function clearComponentArray(id) {
    var ws = this.get('websocket');
    var txt = id + '-clearcomdata';
    this.waitForSocketConnection(ws, function () {
      ws.send(txt);
    });
  },
  sesttimejob: function sesttimejob(content) {
    this.trigger('esttimepub', content.split("-"));
  },
  setstopjob: function setstopjob(content) {
    this.trigger('setstopjobpub', content.split("-"));
  },
  stopprocess: function stopprocess(id) {
    var ws = this.get('websocket');
    var txt = id + "-canceljob";
    this.waitForSocketConnection(ws, function () {
      ws.send(txt);
    });
  },
  jobsch: function jobsch(content) {
    this.trigger('jobschedule', content.split("-"));
  },
  jobschstop: function jobschstop(content) {
    this.trigger('jobschstop', content.split("-"));
  },
  waitForSocketConnection: function waitForSocketConnection(socket, callback) {
    var self = this;

    if (socket.readyState === 1) {
      if (callback != null) {
        callback();
      }
    } else {
      Ember.run.later(function () {
        self.waitForSocketConnection(socket, callback);
      }, 5000);
    }
  }
});

exports["default"] = _default;