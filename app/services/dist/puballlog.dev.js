"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

var _environment = _interopRequireDefault(require("../config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend({
  websocket: new WebSocket(_environment["default"].WEB_HOST),
  alllog: function alllog(content) {
    var ws = this.get('websocket');
    var newdata = "alllog" + "-" + JSON.stringify(content);
    this.waitForSocketConnection(ws, function () {
      ws.send(newdata);
    });
  },
  waitForSocketConnection: function waitForSocketConnection(socket, callback) {
    var self = this;

    if (socket.readyState === 1) {
      console.log("Connection is made");

      if (callback != null) {
        callback();
      }
    } else {
      Ember.run.later(function () {
        self.waitForSocketConnection(socket, callback);
      }, 5);
    }
  }
});

exports["default"] = _default;