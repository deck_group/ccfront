"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

var _environment = _interopRequireDefault(require("../config/environment"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend({
  signout: _environment["default"].API_HOST,
  url: _environment["default"].API_OTHER_HOST,
  websocket: _environment["default"].WEB_HOST
});

exports["default"] = _default;