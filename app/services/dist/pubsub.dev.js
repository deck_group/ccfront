"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports["default"] = void 0;

var _service = _interopRequireDefault(require("@ember/service"));

var _evented = _interopRequireDefault(require("@ember/object/evented"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { "default": obj }; }

var _default = _service["default"].extend(_evented["default"], {
  interestTypepublish: function interestTypepublish(content) {
    this.trigger('interestType', content);
  },
  principalAmtPublish: function principalAmtPublish(content) {
    this.trigger('principalAmt', content);
  },
  ratePublish: function ratePublish(content, object) {
    this.trigger('rate', content, object);
  },
  numberPublish: function numberPublish(content, object) {
    this.trigger('searchNumber', content, object);
  },
  valuePublish: function valuePublish(content, object) {
    this.trigger('valueset', content, object);
  },
  sentdataPublish: function sentdataPublish(content, object) {
    this.trigger('sentdata', content, object);
  },
  flagPublish: function flagPublish(content, object) {
    this.trigger('flagdata', content, object);
  },
  jobPublish: function jobPublish(content) {
    console.log(content);
  },
  conditionalPublish: function conditionalPublish(content) {
    this.trigger('conditional', content);
  }
});

exports["default"] = _default;