import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | predictionreport-param', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:predictionreport-param');
    assert.ok(route);
  });
});
