import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | prediction_batch', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:prediction-batch');
    assert.ok(route);
  });
});
