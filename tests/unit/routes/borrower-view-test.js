import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | borrower-view', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:borrower-view');
    assert.ok(route);
  });
});
