import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';

module('Unit | Route | prediction-log', function(hooks) {
  setupTest(hooks);

  test('it exists', function(assert) {
    let route = this.owner.lookup('route:prediction-log');
    assert.ok(route);
  });
});
